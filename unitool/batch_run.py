#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-

import argparse
import os, re, sys
import utils

from shutil import copyfile
from functools import reduce
from subprocess import Popen, PIPE
from operator import itemgetter

curr_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(curr_path, "../rhsAnalysis"))
from analyze_rhs import analyze_rhs

try:
    import lore
    args   = ("gienah.cs.illinois.edu", "gong15", "polaris", "vectorization")
    cursor = lore.connect(args)
    loop_collections = "/shared/loop_collections_12072018"
except:
    cursor = None

mtn_cache = {}
lid_cache = {}

parser = argparse.ArgumentParser(description='Find symptoms present in a batch of loops.')
parser.add_argument('loops', metavar='loop', type=str, nargs='*', help='loops to process')
parser.add_argument("-i", nargs="*", dest="files", help="list of input files")
parser.add_argument("--nomtn", action='store_true', help="skip printing mutation number")
parser.add_argument("-x", nargs="?", dest="extra", help="extra data to append to output")
parser.add_argument('--copy', action='store_true', help='copy src files to cur dir')
parser.add_argument('--stage', action='store_true', help='stage run (no analysis)')
parser.add_argument('--rhs', action='store_true', help='include rhs analytics')
parser.add_argument('--verbose', action='store_true', help='print debugging information to stderr')
parser.add_argument('--strict', action='store_true', help='enable additional error-checking')
parser.add_argument('--header-only', dest='header_only', action='store_true', help='emit only the header and exit')
args = parser.parse_args()

flattened = []

def read_loop_file(lfile):
    with open(lfile) as f:
        return list(map(fetch_loop_id, filter(bool, map(str.strip, f.readlines()))))        

mtn_ptn  = re.compile(r"(.*\.c)_(.*)_line(\d+)_(\d+)")
name_ptn = re.compile(r"(.*\.c)_(.*)_line(\d+)")
def fetch_loop_id(name, cache_mtn=True):
    if name.endswith(".c"):
        flattened.append((None, name, name))
        return None
    elif ":" in name:
        name = name[(name.index(":") + 1):].strip()
    m = mtn_ptn.search(name) or name_ptn.search(name)
    if m is None:
        return name if "name" not in name else None
    else:
        m = m.groups()
    if m in lid_cache:
        return lid_cache[m]
    query =  """SELECT `table_ptr` FROM loops
                WHERE `file`=%s AND `function`=%s AND `line`=%s
                LIMIT 1"""
    _, lid = cursor.execute(query, m[:3]), cursor.fetchone()[0]
    if cache_mtn:
        if len(m) == 4:
            mtn_cache[lid] = [ m[3] ]
        else:
            mtn_cache[lid] = [ "0" ]
    lid_cache[m] = lid
    return lid

def fetch_loop_info(lid):
    if cursor is None:
        return None
    query = '''SELECT `benchmark`, `version`, `application`, `file`, `function`, `line`
               FROM loops WHERE `table_ptr`=%s'''
    cursor.execute(query, (lid,))
    return cursor.fetchone()

def fetch_num_eligible(lid, xform):
    assert(cursor is not None)
    query = """SELECT COUNT(DISTINCT mutation_number) FROM mutations
               WHERE `id`=%%s AND `%s`<>-1""" % (xform[:-3] + "order")
    _, count = cursor.execute(query, (lid,)), cursor.fetchone()
    return count[0] if count else 0

def fetch_eligible_transformations(lid):
    if cursor is None:
        return [ "interchange", "tiling", "unrolling", "unrolljam" ]
    xforms = []
    if lid is None:
        xforms = [ x[:-4] for x in lore.mutations ]
    else:
        for mut in lore.mutations:
            if fetch_num_eligible(lid, mut):
                xforms.append(mut[:-4])
    return xforms

def generate_permutation(depth, to_move):
    perm = list(range(1, depth + 1))
    perm.remove(to_move + 1)
    perm.append(to_move + 1)
    return tuple(perm)

def check_clu_scores(scores):
    if not scores or None in scores:
        return None
    clu_max = max(scores)
    if clu_max != scores[-1]:
        return generate_permutation(len(scores), scores.index(clu_max))
    else:
        return None

def check_indv_limit(x):
    return x and x.isdigit() and int(x) <= 8

def check_limits(limits):
    small_limits = [ check_indv_limit(x) for x in limits ]
    if any(small_limits) and not small_limits[-1]:
        return generate_permutation(len(scores), \
            next(idx for idx, x in enumerate(small_limits) if x)), small_limits[-1]
    else:
        return None, (small_limits[-1] if small_limits else None)

def fix_permutations(permutations):
    if "(1)" in permutations:
        return None
    permutations = ( eval(perm) for perm in permutations )
    return [ perm for perm in permutations if \
        not all(x < y for x, y in zip(perm, perm[1:])) ]

context_ptn = re.compile(r"CONTEXT: (.*)")
symptom_ptn = re.compile(r"SYMPTOMS: (.*)")
scores_ptn = re.compile(r"SCORES: (.*)")
limits_ptn = re.compile(r"LIMITS: (.*)")
acsmtx_ptn = re.compile(r"ACCESS RECOMMENDATIONS: (.*)")
def run_unitool(src_path):
    env = os.environ.copy()
    if args.strict:
        env[utils.strict_key] = "True"
    process = Popen(['python3', 'unitool.py', src_path], stdout=PIPE, env=env)
    out, err = process.communicate()
    if out and not err:
        out = out.decode('utf-8')
        if (args.verbose):
            print(out, file=sys.stderr)
        m = symptom_ptn.search(out)
        symptoms = eval(m.groups()[0]) if m else []
        m = context_ptn.search(out)
        context = eval(m.groups()[0]) if m else {}
        m = scores_ptn.search(out)
        scores = eval(m.groups()[0]) if m else {}
        m = limits_ptn.search(out)
        limits = eval(m.groups()[0]) if m else {}
        m = acsmtx_ptn.search(out)
        acsmtx = eval(m.groups()[0]) if m else []
        if acsmtx:
            acsmtx = fix_permutations(acsmtx)
        if context and scores:
            scores = [ scores.get(lvl, None) for lvl, _ \
                       in sorted(context.items(), key=itemgetter(1)) ]
        if context and limits:
            limits = [ limits.get(lvl, None) for lvl, _ \
                       in sorted(context.items(), key=itemgetter(1)) ]
        return symptoms, scores, limits, acsmtx
    else:
        return None

loops = set(map(fetch_loop_id, args.loops))
if args.files:
    loops = loops.union(set(reduce(list.__add__, map(read_loop_file, args.files))))
loops = list(filter(bool, loops))

if not len(loops) and (not flattened) and (not args.header_only):
    parser.parse_args(["-h"])

extra_data, extra_cols = dict(), list()
if args.extra:
    from csv import DictReader
    with open(args.extra, 'r') as in_file:
        csv_reader = DictReader(in_file, delimiter=',')
        extra_cols = list(csv_reader.fieldnames)
        for row in csv_reader:
            lid = fetch_loop_id(row["name"], cache_mtn=False)
            extra_data[lid] = row

header = [
    "SourcePath",
    "Src[Sido[Symptoms]]",
    "Src[Expr[CluScore]]",
    "Src[Loop[Limits]]",
    "Src[Xforms[Eligible]]",
    "Src[Sido[Recommendations]]",
    "Src[Ichg[Permutation]]",
    "Src[Loop[SameLimits]]"
]

if (args.rhs):
    header.extend([
        "Src[Expr[RhsOpCount]]",
        "Src[Expr[InitOnly]]",
        "Src[Expr[ScalarReduction]]",
        "Src[Expr[Recurrence]]",
        "Src[Array[Offsets]]"
    ])

std_cols = len(header)
for col in extra_cols:
    if col not in header:
        header.append(col)

print(",".join(header))

if args.header_only:
    sys.exit(0) 

for lid in loops:
    for mid in mtn_cache.get(lid, [ "0" ]):
        info = fetch_loop_info(lid)
        if info is None:
            continue
        name = "_".join(list(info[3:-1]) + ["line" + str(info[-1])])
        if args.stage:
            print(name if args.nomtn else "%s_%s" % (name, mid))
            continue
        loop_path = os.path.join(*([loop_collections] + list(info[:3]) + ["extractedLoops", name + "_loop"]))
        src_path  = os.path.join(*[loop_path + ".c_mutations", name + "_loop.c." + str(mid) + ".c"])
        flattened.append((lid, (name if args.nomtn else "%s_%s" % (name, mid)), src_path))

for (lid, name, src_path) in flattened:
    result = run_unitool(src_path)
    if result is None:
        continue
    symptoms, scores, limits, acsmtx = result
    extra_limits = extra_data.get(lid, {}).get("limits", None)
    if extra_limits and "[[" not in extra_limits:
        extra_limits = eval(extra_limits)
        if len(extra_limits) == len(limits):
            limits = [ str(x) for x in extra_limits ]
    limits_same = len(set(limits)) == 1
    all_small = limits_same and all(map(check_indv_limit, limits))
    eligible = fetch_eligible_transformations(lid)
    recommendations = []
    ichg_symptom_a = check_clu_scores(scores)
    ichg_symptom_b, small_limits_ok = check_limits(limits)
    permutations = []
    if ichg_symptom_b:
        permutations.append(ichg_symptom_b)
    elif ichg_symptom_a and not small_limits_ok:
        permutations.append(ichg_symptom_a)
    ichg_recommended = (permutations or all_small) and "interchange" in eligible
    if acsmtx and ichg_recommended:
        for perm in acsmtx:
            if perm not in permutations:
                permutations.insert(0, perm)
    if ichg_recommended:
        recommendations.append("interchange")
    permutations = str(permutations) if "interchange" in eligible and permutations else ""
    initializer = ("init" in name) or (extra_data.get(lid, {}).get("initializer?", False))
    if (symptoms or initializer) and ("unrolljam" in eligible):
        if not ("interchange" in recommendations and len(symptoms) < 2):
            recommendations.append("unrolljam")
    if not recommendations and "unrolling" in eligible:
        recommendations.append("unrolling")
    out_row = [ symptoms, scores, limits, eligible, recommendations, permutations, limits_same ]
    for col in header[std_cols:]:
        out_row.append(extra_data[lid][col])
    rhs_results = list(analyze_rhs(src_path, True)) if args.rhs else []
    rhs_results = [*rhs_results[:-1], rhs_results[-1] if rhs_results[-1] else "{}"] if rhs_results else []
    if rhs_results:
        print(name, ",".join(map((lambda x : "\"%s\"" % x), (out_row + rhs_results))), sep=",")
    else:
        print(name, ",".join(map((lambda x : "\"%s\"" % x), out_row)), sep=",")
    if args.copy:
        copyfile(src_path, os.path.join(".", os.path.basename(src_path)))

if cursor is not None:
    lore.disconnect()

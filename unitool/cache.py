from statistics import mean
import utils

line_len = 8

class Cache:
    def __init__(self):
        self.hit = set()
        self.all = set() 

    def access(self, idx):
        if idx not in self.all:
            for i in range(line_len):
                self.all.add(idx + i)
        self.hit.add(idx)

def access(cache, lhs, idx):
    if lhs not in cache:
        cache[lhs] = Cache()
    cache[lhs].access(idx)

def compute_reuse(cache):
    scores = []
    for _, val in cache.items():
        score = 1 - len(val.all.difference(val.hit)) / len(val.all)
        scores.append(score)
    return mean(scores)

def build_lhs_and_rhs(expr, itr, val):
    idxs = list(utils.find_all(expr, "["))
    lhs  = expr[:idxs[0]]
    for idx in idxs[:-1]:
        start, end = idx + 1, expr.find("]", idx)
        env = utils.find_locals(expr[start:end])
        if itr in env:
            env[itr] = val
            lhs += "[{0}]".format(eval(expr[start:end], None, env))
        else:
            lhs += "[{0}]".format(expr[start:end])
    rhs = expr[(idxs[-1] + 1):-1]
    env = utils.find_locals(rhs)
    env[itr] = val
    rhs = eval(rhs, None, env)
    return lhs, rhs

def simulate_access(ctx, stride, cache, expr):
    itr = utils.get_current_iterator(ctx)
    for i in range(line_len):
        access(cache, *build_lhs_and_rhs(expr, itr, i * stride))

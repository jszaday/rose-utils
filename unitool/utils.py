import io, os, re
import itertools
import subprocess
import cache
import operator

from functools import reduce
from sympy import simplify
from sympy.parsing.sympy_parser import parse_expr

strict_key = "UNITOOL_STRICT"

main_path = os.path.dirname(os.path.realpath(__file__))
main_exe = os.path.join(main_path, "../loopUtil/main")
perm_exe = os.path.join(main_path, "../permute/main")

def find_permutables(src_file, info):
    permutables = None
    for line in run_binary([ perm_exe, src_file ]):
        if line.startswith('LOOP'):
            lid = line[5:line.index(':')]
            info[lid] = info.get(lid, []) + \
                [ x.strip() for x in line[line.index(':') + 2:].split(",") ]
        elif line.startswith('PERMUTABLE: '):
            permutables = line[12:].strip()
    return permutables

def find_permutations(src_file, info):
    permutables = find_permutables(src_file, info)
    permutables = permutables.split('),')
    permutations = []
    for permutable in permutables:
        if '(' in permutable:
            permutable = permutable.split('(', 1)[1]
            permutable = permutable.split(',')
            permutable = [ int(v) for v in permutable ]
            cnt_permutations = list(itertools.permutations(permutable))
            if not permutations:
                permutations = cnt_permutations
            else:
                permutations = itertools.product(cnt_permutations)
    # filter out trivial permutations
    permutations = [ perm for perm in permutations \
        if not all(x < y for x, y in zip(perm, perm[1:])) ]
    return permutations

def run_binary(argv):
    with subprocess.Popen(argv, stdout=subprocess.PIPE) as proc:
        for line in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
            yield line
        os.waitpid(proc.pid, 0)

def safe_search(s, ptn):
    m = ptn.search(s)
    return m.groups() if m else None

def multi_search(s, *ptns):
    for ptn in ptns:
        m = safe_search(s, ptn)
        if m:
            return m
    return None

def head_or_none(t):
    return t[0] if t else None

ctx_ptn = re.compile(r"LoopCtx\(.*?,(.*?),(.*)\)#(\d+)")
def parse_ctx(ctx, ret):
    g = safe_search(ctx, ctx_ptn)
    if not g:
        return None
    ret[g[0]] = int(g[2])
    return ret if g[1] == "NULL" else parse_ctx(g[1], ret)

ident_ptn = re.compile(r"[a-zA-Z_][a-zA-Z0-9_]*")
def find_locals(s):
    return { key : cache.line_len for key in ident_ptn.findall(s) }

def find_offsets(idx, itr):
    offset_ptn = re.compile(itr + r"\s*([+-])\s*(\d+)")
    offset = safe_search(idx, offset_ptn)
    return [ offset[0] + offset[1] ] if offset else []

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches

def get_current_iterator(ctx):
    return head_or_none([
        key for key in ctx if ctx[key] == max(ctx.values()) ])

def format_range(start, end, step):
    try:
        return str(simplify(parse_expr("(" + end + " - " + start + " + 1) / " + step)))
    except:
        return None

def format_ordering(ctx, *argv):
    argv = list(map(str, argv))
    argv = (['*'] * (len(ctx) - len(argv))) + argv
    return "({})".format(",".join(argv))

def prune_accesses(accesses):
    for l in accesses.keys():
        if len(accesses[l]) > 1:
            accesses[l][1] += accesses[l][0]
            accesses[l].pop(0)

def access_ordering(ctx, accesses, curr):
    to_place = list(accesses.keys())
    ordering = []
    while len(to_place) > 1:
        if not curr:
            while all(accesses[x][0] == 0 for x in accesses.keys()):
                prune_accesses(accesses)
            curr = max(accesses.keys(), key=lambda loop: accesses[loop][0])
            dups = [ x for x in accesses.keys() if x != curr and accesses[x][0] == accesses[curr][0] ]
            if any(dups):
                sub_orderings = reduce(operator.add, \
                    (access_ordering(ctx, dict(accesses), x) for x in (dups + [curr])))
                return [ ordering + x for x in sub_orderings ]
        ordering.append(ctx[curr])
        to_place.remove(curr)
        del accesses[curr]
        prune_accesses(accesses)
        curr = None
    ordering.append(ctx[accesses.popitem()[0]])
    return [ ordering ]

def analyze_access_matrix(ctx, accesses):
    return [ format_ordering(ctx, *reversed(x)) for x in reversed(access_ordering(ctx, accesses, None)) ]

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import io, re, os, sys
import subprocess
import utils
import sys

from operator import itemgetter
from cache import simulate_access, compute_reuse

ctx_ptn = re.compile(r"(START|END): (.*)")
access_ptn = re.compile(r"(ACCESS): (.*)")
matrix_ptn = re.compile(r"ACCESS (SUMMARY):$")
# range_ptn = re.compile(r"(RANGE): \[(.*?),\s*(.*?)\]")
range_ptn  = re.compile(r"(RANGE): \[(.*?)\s*(,|:.*?:)\s*(.*?)\]")
index_ptn = re.compile(r"\[(.*?)\]")
mat_ptn = re.compile(r"(.*?)\t([\d\t]+)")
depths = dict()
offsets = dict()

global strict
strict = os.getenv(utils.strict_key, "False") != "False"

def parse_access(ctx, access):
    depth = 1
    while True:
        idx = utils.head_or_none(utils.safe_search(access, index_ptn))
        if not idx:
            break
        itrs = [ itr for itr in ctx if itr in idx ]
        for itr in itrs:
            offsets[itr] = offsets.get(itr, set()).union(utils.find_offsets(idx, itr))
            depths[itr] = depths.get(itr, set()).union([ depth ])
        access = access[(access.index("]") + 1):]
        depth += 1

def count_irregular_accesses(ctx, depths):
    cnt = 0
    for itr in ctx:
        if itr not in depths:
            global strict
            if strict:
                raise RuntimeError("ERROR: could not find %s in %s." % (itr, depths))
            else:
                continue
        for depth in depths[itr]:
            # If we are the last level, we are free to operate on
            # later indicies (without it being an irregularity)
            if ctx[itr] == max(ctx.values()) and depth > ctx[itr]:
                continue
            if depth != ctx[itr]:
                cnt += 1
    return cnt

# Check environment and command-line arguments
verbose = bool(int(os.environ.get("VERBOSE", "0")))
irregular_threshold = 1

if len(sys.argv) < 2 or not sys.argv[1].endswith(".c"):
    print("ERROR: Expected usage is: %s <.c file> <optional?>" % sys.argv[0], file=sys.stderr)
    sys.exit(-1)

# Run the loop utility, iterating over its output
ctx, saved_ctx = None, None
saving, first = False, True
symptoms = set()
limits = dict()
scores = dict()
strides = dict()
for line in utils.run_binary([ utils.main_exe ] + sys.argv[1:]):
    if verbose:
        print(line, end="")
    if saving:
        m = mat_ptn.search(line)
        g = m.groups() if m is not None else None
        if g is not None:
            if g[0]:
                accesses[g[0]] = [ int(x) for x in g[1].split("\t") if x ]
            continue
        else:
            saving = False
    m = utils.multi_search(line, ctx_ptn, access_ptn, matrix_ptn, range_ptn)
    if not m:
        continue
    elif m[0] == "START":
        ctx = utils.parse_ctx(m[1], {})
        accesses = dict()
        cache, depths, offsets = \
            dict(), dict(), dict()
    elif m[0] == "END":
        if cache:
            scores[utils.get_current_iterator(ctx)] = compute_reuse(cache)
        if first:
            saved_ctx = ctx
            print("CONTEXT:", ctx)
            if count_irregular_accesses(ctx, depths) >= irregular_threshold:
                symptoms.add("A")
            if any(len(s) >= 1 for s in offsets.values()):
                symptoms.add("B")
            print("OFFSETS:", offsets)
            print("ACCESS DEPTHS:", depths)
        if accesses:
            print("ACCESS RECOMMENDATIONS:", utils.analyze_access_matrix(ctx, accesses))
        ctx, first = None, False
    elif m[0] == "ACCESS" and ctx:
        if first:
            parse_access(ctx, m[1])
        stride = strides[utils.get_current_iterator(ctx)]
        simulate_access(ctx, stride, cache, m[1])
    elif m[0] == "RANGE":
        stride = m[2]
        stride = "1" if ":" not in stride else stride[1:-1]
        formatted = utils.format_range(m[1], m[3], stride)
        strides[utils.get_current_iterator(ctx)] = int(stride)
        limits[utils.get_current_iterator(ctx)] = formatted
    elif m[0] == "SUMMARY":
        saving = True

if limits:
    print("LIMITS:", limits)
    if saved_ctx:
        limits = [ limits.get(lvl, None) for lvl, _ \
                   in sorted(saved_ctx.items(), key=itemgetter(1)) ]
        if limits[-1] and limits[-1].isdigit() and \
            len(saved_ctx) >= 3 and int(limits[-1]) <= 8:
            symptoms.add("C")

if symptoms:
    print("SYMPTOMS:", list(sorted(symptoms)))

if scores:
    print("SCORES:", scores)

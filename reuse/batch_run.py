#!/usr/bin/python3
# -*- coding: utf-8 -*-
import lore
import argparse
import os, re

from functools import reduce
from subprocess import Popen, PIPE

args   = ("192.17.58.228", "szaday2", "polaris", "vectorization")
cursor = lore.connect(args)
mtn_cache = {}
loop_collections = "/shared/loop_collections_12072018"

parser = argparse.ArgumentParser(description='Calculate the reuse score for a batch of loops.')
parser.add_argument('loops', metavar='loop', type=str, nargs='*', help='loops to process')
parser.add_argument("-i", nargs="*", dest="files", help="list of input files")
args = parser.parse_args()

def read_loop_file(lfile):
    with open(lfile) as f:
        return list(map(fetch_loop_id, filter(bool, map(str.strip, f.readlines()))))

mtn_ptn = re.compile(r"^(.*\.c)_(.*)_line(\d+)_(\d+)$")
name_ptn = re.compile(r"^(.*\.c)_(.*)_line(\d+)$")
def fetch_loop_id(name):
    m = (mtn_ptn.match(name) or name_ptn.match(name)).groups()
    query =  """SELECT `table_ptr` FROM loops
                WHERE `file`=%s AND `function`=%s AND `line`=%s
                LIMIT 1"""
    _, lid = cursor.execute(query, m[:3]), cursor.fetchone()[0]
    if len(m) == 4:
        mtn_cache[lid] = [ m[3] ]
    else:
        mtn_cache[lid] = [ "0" ]
    return lid

def fetch_loop_info(lid):
    query = '''SELECT `benchmark`, `version`, `application`, `file`, `function`, `line`
               FROM loops WHERE `table_ptr`=%s'''
    cursor.execute(query, (lid,))
    return cursor.fetchone()

def run_cache_sim(src_path):
    rose_process = Popen(['./main', '-rose:skip_unparse', '-rose:skipfinalCompileStep', src_path], stdout=PIPE)
    sim_process  = Popen(['python3', 'cache_sim.py'], stdin=rose_process.stdout, stdout=PIPE)
    rose_process.stdout.close() # enable write error in dd if ssh dies
    out, err = sim_process.communicate()
    if len(out) and not err:
        return float(out.decode("utf-8").strip())
    else:
        return None

loops = set(map(fetch_loop_id, args.loops))
if args.files:
    loops = loops.union(set(reduce(list.__add__, map(read_loop_file, args.files))))

for lid in loops:
    for mid in mtn_cache[lid]:
        info = fetch_loop_info(lid)
        name = "_".join(list(info[3:-1]) + ["line" + str(info[-1])])
        loop_path = os.path.join(*([loop_collections] + list(info[:3]) + ["extractedLoops", name + "_loop"]))
        src_path  = os.path.join(*[loop_path + ".c_mutations", name + "_loop.c." + str(mid) + ".c"])
        print("%s_%s" % (name, mid), run_cache_sim(src_path))

lore.disconnect()

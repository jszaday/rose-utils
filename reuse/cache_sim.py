#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re

from statistics import mean
from sys        import stdin

cache = dict()
access_ptn = re.compile(r"ACCESS: (.*)\[(.*)\]")
ident_ptn  = re.compile(r"[a-zA-Z_][a-zA-Z0-9_]*")

class Cache:
    def __init__(self):
        self.hit = set()
        self.all = set() 

    def access(self, idx):
        if idx not in self.all:
            for i in range(8):
                self.all.add(idx + i)
        self.hit.add(idx)

def access(lhs, idx):
    if lhs not in cache:
        cache[lhs] = Cache()
    cache[lhs].access(idx)

def compute_reuse():
    scores = []
    for key, val in cache.items():
        score = 1 - len(val.all.difference(val.hit)) / len(val.all)
        scores.append(score)
    print(mean(scores))

def find_locals(s):
    return { key : 8 for key in ident_ptn.findall(s) }

for line in stdin:
  m = access_ptn.search(line)
  g = m.groups() if m else None
  if g:
    access(g[0], eval(g[1], None, find_locals(g[1])))

compute_reuse()

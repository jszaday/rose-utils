#include <rose.h>
#include <iostream>

Sg_File_Info *kFileInfo;
SgNode       *kRootNode;

void replaceRefs( SgExpression *ctx, SgVariableSymbol *oldSym, SgExpression* newExp ) {
  std::vector<SgVarRefExp*> refs;
  SageInterface::collectVarRefs(ctx, refs);
  for (auto ref : refs) {
    if (ref->get_symbol() == oldSym) {
      auto parent = ref->get_parent();
      if (isSgExpression(parent)) {
        auto end = ref->get_endOfConstruct();
        ((SgExpression*)parent)->replace_expression(ref, newExp);
        newExp->set_endOfConstruct(end);
      }
    }
  }
}

class LoopCtx
{
  SgForStatement* mStmt;
  LoopCtx* mParent;
  std::vector<SgVariableSymbol*> mIters;
public:
  LoopCtx( SgForStatement* stmt, LoopCtx* parent = nullptr )
  : mStmt(stmt), mParent(parent) {
    for (SgStatement* init : stmt->get_init_stmt()) {
      if (isSgExprStatement(init)) {
        auto expr = ((SgExprStatement*)init)->get_expression();
        if (isSgAssignOp(expr)) {
          auto lhs = ((SgAssignOp*)expr)->get_lhs_operand();
          if (isSgVarRefExp(lhs)) {
            mIters.push_back(((SgVarRefExp*)lhs)->get_symbol());
          }
        }
      }
    }
  }

  SgForStatement* get_for_statement() const {
    return mStmt;
  }

  LoopCtx* get_parent() const {
    return mParent;
  }

  bool is_iterator(SgVariableSymbol* sym) const {
    return std::find(mIters.begin(), mIters.end(), sym) != mIters.end();
  }

  SgVariableSymbol* get_iterator() const {
    if (mIters.size() == 1) {
      return mIters[0];
    } else {
      return nullptr;
    }
  }

  std::string to_string() const {
    std::stringstream ss;
    ss << "LoopCtx([" ;
    for (auto sym : mIters) {
      ss << sym->get_name().getString() << ",";
    }
    ss << "],";
    if (mParent) {
      ss << mParent->to_string();
    } else {
      ss << "NULL";
    }
    ss << ")";
    return ss.str();
  }
};

std::ostream& operator<<(std::ostream &strm, const LoopCtx &ctx) {
  return strm << ctx.to_string();
}

class RoseVisitor : public ROSE_VisitorPattern
{
public:
  RoseVisitor( ) { }

  void visit( SgFunctionDefinition *fnDef ) {
    // Grab the function's declaration
    SgFunctionDeclaration *fnDecl = fnDef->get_declaration();
    // Ensure that the function is actually contained in the target file
    if (!kFileInfo->isSameFile(fnDecl->get_file_info())) return;
    // Find all of the for loops in the function's body
    auto loops = NodeQuery::querySubTree(fnDef->get_body(), V_SgForStatement);
    // For each loop found:
    for (auto node : loops) {
      // Search for an enclosing loop
      auto encl = SageInterface::findEnclosingLoop((SgStatement*)node->get_parent());
      // If one is not found
      if (!encl) {
        // Assume it is a top-level loop and visit the loop nest
        this->visit((SgForStatement*)node);
      }
    }
  }

  void visit( SgForStatement *stmt, LoopCtx *parent = nullptr ) {
    SageInterface::forLoopNormalization(stmt);
    LoopCtx ctx(stmt, parent);
    auto body = isSgBasicBlock(stmt->get_loop_body())->get_statements();
    std::cout << "START: " << ctx.get_iterator()->get_name().getString() << std::endl;
    for (SgStatement *stmt : body) {
      if (isSgForStatement(stmt)) {
        this->visit((SgForStatement*)stmt, &ctx);
      } else {
        std::vector<SgVarRefExp*> refs;
        SageInterface::collectVarRefs(stmt, refs);
        for (auto ref : refs) {
          if (ctx.is_iterator(ref->get_symbol())) {
            visit((SgVarRefExp*)ref, &ctx);
          }
        }
      }
    }
  }

  void visit( SgVarRefExp *ref, LoopCtx *ctx ) {
    auto next = ref->get_parent();
    SgNode* top = nullptr;
    while (!isSgStatement(next)) {
      if (isSgPntrArrRefExp(next)) {
        top = next;
      } else if (top) {
        break;
      }
      next = next->get_parent();
    }
    if (top) {
      visit((SgPntrArrRefExp*) top, ctx);
    }
  }

  void visit( SgPntrArrRefExp *exp, LoopCtx *ctx ) {
    for (int i = 0; i < 8; i++) {
      auto val  = new SgIntVal(kFileInfo, i);
      auto copy = SageInterface::copyExpression(exp);
      replaceRefs(copy, ctx->get_iterator(), val);
      std::cout << "ACCESS: " << copy->unparseToString() << std::endl;
    }
  }
};

int main (int argc, char* argv[]) {
  SgProject* project = frontend(argc, argv);
  ROSE_ASSERT( project != NULL );

  kFileInfo = project->get_file(0).get_file_info();
  kRootNode = project;

  SageInterface::changeAllBodiesToBlocks( project );

  RoseVisitor visitor;
  traverseMemoryPoolVisitorPattern( visitor );

  return backend( project );
}

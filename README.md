# Getting Started w/ LORE Rose Utils

This guide omits `sudo` (since it's not used in Docker), include it when necessary.

We recommend building within a docker container:

    docker pull ubuntu
    docker run -it ubuntu bash

That being said, you can follow this procedure on any Ubuntu machine (see [ROSE's wiki](https://github.com/rose-compiler/rose/wiki/How-to-Set-Up-ROSE) for other install guides):

    apt-get update
    apt-get install software-properties-common
    add-apt-repository ppa:rosecompiler/rose-development
    apt-get install git g++ make rose wget

    wget https://bootstrap.pypa.io/get-pip.py
    python3 get-pip.py
    python3 -m pip install sympy

If you installed ROSE from APT, you will need to set `ROSE_HOME` using:

    export ROSE_HOME=/usr/rose/

Otherwise, if you built ROSE manually, set it to your ROSE installation path.

If your build of ROSE has Fortran language support enabled, you will need to set the `JAVA_HOME` environment variable as well, for example:

    export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

After setup is complete, clone this repo and run its setup script (i.e., `build.sh`):

    git clone https://gitlab.com/Wingpad/rose-utils
    cd rose-utils
    ./build.sh

NOTE: You can ignore any warnings about `JAVA_HOME` if your build is successful.

If you prefer to build things manually (without `build.sh`), you can call `make` within the following directories:

- `permute/`
- `rhsAnalysis/`
- `loopUtil/`

If you're using Docker, you should now [commit the container](https://docs.docker.com/engine/reference/commandline/commit/).

When you're ready to run, use `unitool`:

    cd rose-utils/unitool
	python3 batch_run.py --rhs (loop.c)
	 
	# sample output	
    name,symptoms,scores,limits,eligible,recommendations,permutation,limits_same,rhs_op_count,init_only,scalar_reduction,recurrence,offsets
    loop.c,"[]","[1.0]","{}","['interchange', 'tiling', 'unrolling', 'unrolljam']","['unrolling']","","False","1","False","True","False","{}"

You can use a loop file which contains a list of filenames as well (typically ends in .lf, one loop filename per line); pass them using `-i`.

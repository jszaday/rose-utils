import os
import glob
import shutil
import subprocess

from more_itertools import peekable
from fileinput import FileInput

libs    = '-lmeasure -lcrypto -lssl -lisolatedLoopUtility'
repo    = os.getenv('LORE_HOME', '/shared/loop_collections_10242017')
common  = os.getenv('LORE_COMMON', '/shared/common')

def collect_declarations(loopInfo):
    loopName = "_".join((loopInfo[3:-1] + ("line" + str(loopInfo[-1]),)))
    srcPath  = os.path.join(*((repo,) + loopInfo[:3] + ("extractedLoops", loopName + "_loop.c_mutations", loopName + "_loop.c.0.c")))
    cntdown  = -1
    saved    = ""
    with open(srcPath, "r") as srcFile:
        for line in srcFile:
            if "void loop()" in line:
                cntdown = 1
            elif cntdown > 0:
                cntdown -= 1
            elif cntdown == 0:
                if "#pragma scop" in line:
                    cntdown = -1
                else:
                    saved += line
    return saved

def make_driver(loopInfo, outPath, template, limits):
    loopName = "_".join((loopInfo[3:-1] + ("line" + str(loopInfo[-1]),)))
    mainPath = os.path.join(*((repo,) + loopInfo[:3] + ("extractedLoops", loopName + "_main.c")))
    mnFlePth = os.path.join(outPath, loopName + "_main.c")

    variables = "extern void* dummy(int num, ...);\n"
    data = ""
    checksum = ""
    define = ""
    include = "#include <string.h>\n#include <openssl/md5.h>\n"

    ldVars = False
    ldData = False
    ldChk = False
    addedData = False

    inputDataFileName = ""

    with open(mainPath, "r") as mainFile:
        iter = peekable(mainFile)
        while True:
            try:
                line = next(iter).strip()
                if (line == "extern void* dummy(int num, ...);"):
                    ldVars = True
                    continue
                elif (line == "int main(int argc, char** argv)"):
                    ldVars = False
                elif (line == "for (int it = 0; it < iterations; it++)"):
                    ldData = True
                    next(iter)
                    continue
                elif (line == "if (it < 1)" and "cycleArray = getCycleArray" in iter.peek()):
                    next(iter)
                    continue
                elif (line == "deleteCycleArray(cycleArray);"):
                    ldChk = True
                    continue
                elif (line == "printf(\"Simulation time (cycles) stats:\\n min\\tmax\\tmean\\tmedian\\tstdv\\n\");"):
                    ldChk = False
                elif (loopName in line and "include" in line):
                    include += line +"\n"
                elif (line == "tsc_start = rdtsc();"):
                    ldData = False
                elif ("static char* inputDataFileName[] = " in line):
                    inputDataFileName = line
                elif (("resetHeapData" in line or "mallocMemoryChunk" in line or "StackToCurrentStack" in line) and (not "extern" in line) and (not addedData)):
                    addedData = True
                    data = """
void *oldBrk;
static char heapDataFile[256] = {0};
static char stackDataFile[256] = {0};
static char globalDataFile[256] = {0};
sprintf (heapDataFile, "%s/%s", INPUT_DIR, inputDataFileName[0]);
sprintf (stackDataFile, "%s/%s", INPUT_DIR, inputDataFileName[1]);
sprintf (globalDataFile, "%s/%s", INPUT_DIR, inputDataFileName[2]);""" + data;
                    variables = variables + """
extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length, int iterations);
extern void* saveCurrentStackToHeap(void* curSP, void* preBottom, long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);
"""

                if (ldVars):
                    variables += line + "\n"
                elif (ldData):
                    if ("argv[1]" in line):
                        line = line.replace("argv[1]", "path")
                    elif (line == "exit(-1);"):
                        line = "return -1;"
                    data += line + "\n"
                elif (ldChk):
                    checksum += line + "\n"

            except StopIteration:
                break

    if (data):
        data = "int inputIndices[10];\nFILE* pFile_;\n" + data;
        data += "return 0;\n"
        define += "#define HAS_DATA\n"

    if (checksum):
        checksum += "return 0;\n"
        define += "#define HAS_CHKSUM\n"

    shutil.copyfile(template, mnFlePth)

    decls  = collect_declarations(loopInfo)
    limits = decls + "\n".join( """printf("%%d\\n", %s);""" % limit for limit in limits )

    repls = {
        "<data>": inputDataFileName + data,
        "<variables>": variables,
        "<define>": define,
        "<include>": include,
        "<limits>": limits,
        "read_codelet_data(0, path)": "read_codelet_data(0, INPUT_DIR)"
    }

    with FileInput(mnFlePth, inplace=True) as drvFile:
        for line in drvFile:
            printed = False
            for (term, repl) in repls.items():
                if (term in line):
                    print(line.replace(term, repl), end='')
                    printed = True
            if (not printed):
                print(line, end='')

    return mnFlePth

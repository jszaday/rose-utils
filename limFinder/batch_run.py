#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
import lore, sys
import os, re
import subprocess

from sympy import simplify
from sympy.parsing.sympy_parser import parse_expr

args = ("gienah.cs.illinois.edu", "szaday2", "polaris", "vectorization")
cursor = lore.connect(args)
loop_collections = "/shared/loop_collections_12072018"

main_path = os.path.dirname(os.path.realpath(__file__))
main_exe  = os.path.join(main_path, "..", "loopUtil", "main")

mtn_cache = dict()

start_ptn  = re.compile(r"(START): LoopCtx\((.*?),(.*?),.*\)#(\d+)")
end_ptn    = re.compile(r"(END): LoopCtx\(.*?,(.*?),.*\)#(?:\d+)")
range_ptn  = re.compile(r"(RANGE): \[(.*?)\s*(,|:.*?:)\s*(.*?)\]")

mtn_ptn  = re.compile(r"^(.*\.c)_(.*)_line(\d+)_(.*)$")
name_ptn = re.compile(r"^(.*\.c)_(.*)_line(\d+)$")
def fetch_loop_id(name):
    if ':' in name:
        name = name[(name.index(':') + 1):].strip()
    m = mtn_ptn.match(name) or name_ptn.match(name)
    g = m.groups() if m else None
    mid = g[3] if g and len(g) == 4 else None
    if not m:
        return name
    query =  """SELECT `table_ptr` FROM loops
                WHERE `file`=%s AND `function`=%s AND `line`=%s
                LIMIT 1"""
    _, lid = cursor.execute(query, g[:3]), cursor.fetchone()
    lid = lid[0] if lid else None
    if lid and mid:
        mtn_cache[lid] = mtn_cache.get(lid, []) + [ mid ]
    return lid

def read_loop_file(in_file):
    with open(in_file) as f:
        return list(map(fetch_loop_id, \
            filter(bool, map(str.strip, f.readlines()))))

def fetch_loop_info(lid):
    query = '''SELECT `benchmark`, `version`, `application`, `file`, `function`, `line`
               FROM loops WHERE `table_ptr`=%s'''
    cursor.execute(query, (lid,))
    return cursor.fetchone()

def format_range(start, end, stride="1"):
    return str(simplify(parse_expr("(%s - %s + 1) / %s" % (end, start, stride))))

loops = []
for arg in sys.argv[1:]:
    loops.extend(read_loop_file(arg))

for loop in set(loops):
    info = fetch_loop_info(loop)
    if not info:
        continue
    name = "_".join(list(info[3:-1]) + ["line" + str(info[-1])])
    for mutation in mtn_cache.get(loop, ['0']):
        loop_path = os.path.join(*([loop_collections] + list(info[:3]) + ["extractedLoops", name + "_loop"]))
        src_path  = os.path.join(*[loop_path + ".c_mutations", name + "_loop.c.%s.c" % mutation])
        try:
            output = subprocess.check_output([ main_exe, src_path ]).decode("utf-8")
        except:
            continue
        limits = dict()
        for line in map(str.strip, output.split("\n")):
            m = start_ptn.search(line) or end_ptn.search(line) or \
                range_ptn.search(line)
            g = m.groups() if m else (None,)
            if g[0] == "START":
                last_ctx = int(g[3])
            elif g[0] == "END":
                limits[last_ctx - 1] = limits.get(last_ctx - 1, None)
                last_ctx = None
            elif g[0] == "RANGE":
                try:
                    stride = g[2]
                    stride = "1" if ":" not in stride else stride[1:-1]
                    limits[last_ctx - 1] = format_range(g[1], g[3], stride)
                except:
                    pass
        print("%s_%s" % (name, mutation), [limits[i] for i in sorted(limits.keys())])

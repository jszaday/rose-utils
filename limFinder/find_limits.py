#!/usr/bin/python3
import lore
import os
import io
import re
import subprocess

from shutil import copyfile
from sys    import stdin
from make_codelet import make_driver, repo

# Hostname and User Information for the LORE DB 
args   = ("192.17.58.228", "szaday2", "polaris", "vectorization")
cursor = lore.connect(args)

# Patterns for matching LORE names, with or without a mutation number
line_ptn = re.compile(r"^(.*?) (.*?)\s*$")
name_ptn = re.compile(r"^(.*\.c)_(.*)_line(\d+)_?(\d*)$")
int_ptn  = re.compile(r"^(\d+)$")

def get_loop_id_for_name(loop_name):
    query =  '''SELECT `table_ptr` FROM loops
                WHERE `file`=%s AND `function`=%s AND `line`=%s
                LIMIT 1'''
    _, loop_id = cursor.execute(query, loop_name), cursor.fetchone()
    return loop_id[0] if loop_id is not None else None

def get_loop_info_for_id(loop_id):
    query = '''SELECT `benchmark`, `version`, `application`, `file`, `function`, `line`
               FROM loops
               WHERE `table_ptr`=%s'''
    cursor.execute(query, (loop_id,))
    return cursor.fetchone()

cc        = os.getenv("CC", "icc")
common    = os.getenv("COMMON_HOME", "/shared/common")
cflags    = os.getenv("CFLAGS", "") + " -std=c99 -I{0} -I/usr/local/include -I/usr/include/x86_64-linux-gnu".format(common)
ldflags   = os.getenv("LIBS", "") + " -lcrypto -lssl -L{0} {0}/libisolatedLoopUtility.so {0}/libmsrapi.a -lm {0}/dummy.o".format(common)

try:
    os.chdir("build")
except:
    pass

def compile_and_run_codelet(main_path, data_path, loop_name):
    exec_name = loop_name + ".run"
    cc_args = [cc, main_path, "-o", exec_name, "-I", data_path, "-DINPUT_DIR=\"" + data_path + "\""] + (cflags + ldflags).split()
    # print(" ".join(cc_args))
    status = subprocess.call(cc_args)
    if (status != 0):
        return None
    proc = subprocess.Popen([ "./" + exec_name ], stdout=subprocess.PIPE)
    results = []
    for line in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
        m = int_ptn.match(line)
        if m:
            results.append((m.groups())[0])
    os.waitpid(proc.pid, 0)
    return results

def eval_limits_for_codelet(lid, limits):
    if "None" in limits:
        return limits
    limits    = [ x.strip() for x in limits[1:-1].split(",") ]
    info      = get_loop_info_for_id(lid)
    name = "_".join(list(info[3:-1]) + ["line" + str(info[-1])])
    data_path = os.path.join(*([ repo ] + list(info[:3]) + ["input"]))
    main_path = make_driver(info, ".", "../drvtemplate.c", limits)
    results   = compile_and_run_codelet(main_path, data_path, name)
    if len(results) == len(limits):
        return ", ".join(results)
    else:
        return ", ".join(limits)

for line in stdin:
    m = line_ptn.match(line)
    g = m.groups() if m else None
    if g is None:
        continue
    m = name_ptn.match(g[0])
    name = m.groups() if m else None
    if name is None:
        continue
    lid = get_loop_id_for_name(name[:-1])
    limits = eval_limits_for_codelet(lid, g[1].replace('\'','').replace('"', ''))
    print("%s\t[%s]" % (g[0], limits))

lore.disconnect()

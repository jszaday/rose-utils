#include <stdio.h>
<include>

<define>

<variables>

int read_codelet_data(int it, char *path) {
#ifdef HAS_DATA
  <data>
#else
  return 0;
#endif
}

int main(void) {
  int  rc;
  char path[256];

  if (rc = read_codelet_data(0, path)) {
    printf("FATAL: Could not read input data, terminating with %d.\n", rc);
    return rc;
  }

  <limits>

  printf("SUCCESS: Codelet terminating normally.\n");

  return 0;
}

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
from sys import stdin
from sympy import simplify
from sympy.parsing.sympy_parser import parse_expr

range_ptn = re.compile(r"RANGE: \[(.*?),\s*(.*?)\]")

def format_range(start, end):
    return str(simplify(parse_expr(end + " - " + start)))

limits = list()
for line in stdin:
    m = range_ptn.search(line)
    g = m.groups() if m else None
    if g:
        limits.append(format_range(*g))

print(limits)

#include <rose.h>
#include <iostream>
#include <fstream>
#include <tuple>

class LoopCtx
{
  LoopCtx *mParent;
  LoopCtx *mTopLevel;
  SgForStatement *mStmt;
  SgVariableSymbol *mIterator;
  SgExpression *mLowerBound;
  SgExpression *mUpperBound;
  SgExpression *mStride;
  int mUid, mDepth;
  std::vector<std::vector<size_t>> mAccesses;
  std::vector<std::tuple<size_t, SgPntrArrRefExp*>> mArrRefs;
public:
  LoopCtx( SgForStatement* stmt, LoopCtx* parent = nullptr )
  : mStmt(stmt), mParent(parent) {
    SageInterface::forLoopNormalization(mStmt);
    auto isCanonicalForLoop = SageInterface::isCanonicalForLoop(mStmt);
    if (!isCanonicalForLoop) {
      std::cerr << "WARNING: non canonical for loop: " << mStmt->unparseToString() << std::endl;
    }
    SageInterface::getForLoopInformations(mStmt, mIterator, mLowerBound, mUpperBound, mStride);
    mUid   = ++kUidCtr;
    mDepth = parent ? (parent->mDepth + 1) : 1;
    mTopLevel = parent ? parent->mTopLevel : this;
    mAccesses.resize(get_depth());
  }

  void add_arr_ref(SgPntrArrRefExp* exp) {
    mTopLevel->mArrRefs.push_back(std::make_tuple(mDepth, exp));
  }

  bool has_arr_refs() const {
    return mTopLevel->mArrRefs.size() > 0;
  }

  const std::vector<std::tuple<size_t, SgPntrArrRefExp*>>& get_arr_refs() const {
    return mTopLevel->mArrRefs;
  }

  SgForStatement* get_for_statement() const {
    return mStmt;
  }

  LoopCtx* get_parent() const {
    return mParent;
  }

  int get_depth() const {
    return mDepth;
  }

  bool is_iterator(SgVariableSymbol* sym) const {
    return sym == mIterator;
  }
  
  SgExpression* get_lower_bound() { return mLowerBound; }
  SgExpression* get_upper_bound() { return mUpperBound; }
  SgExpression* get_stride() { return mStride; }
  SgBasicBlock* get_loop_body()   { return isSgBasicBlock(mStmt->get_loop_body()); }

  int find_iterator_depth(SgVariableSymbol* sym) const {
    if (is_iterator(sym)) {
      return get_depth();
    } else if (mParent) {
      return mParent->find_iterator_depth(sym);
    } else {
      return 0;
    }
  }

  SgVariableSymbol* get_iterator() const {
    return mIterator;
  }

  std::string get_iterator_string() const {
    return mIterator->get_name().getString();
  }

  std::string get_location() const {
    auto fNode = AbstractHandle::buildroseNode(mStmt);
    auto fName = fNode->getFileName();
    auto idx   = fName.find_last_of("/");
    return fName.substr(idx + 1, std::string::npos) + ":" + std::to_string(fNode->getStartPos().line);
  }

  std::string to_string() const {
    std::stringstream ss;
    ss << "LoopCtx(" ;
    ss << get_location() << ",";
    ss << mIterator->get_name().getString() << ",";
    if (mParent) {
      ss << mParent->to_string();
    } else {
      ss << "NULL";
    }
    ss << ")#" << mUid;
    return ss.str();
  }

  void access(int iterDepth, int arrDepth) {
    auto& tmp = mAccesses[iterDepth - 1];
    if (tmp.size() < arrDepth) {
      auto prev = tmp.size();
      tmp.resize(arrDepth);
      for (auto i = prev; i < arrDepth; i++) {
        tmp[i] = 0;
      }
    }
    tmp[arrDepth - 1] += 1;
  }

  std::string access_matrix() const {
    auto max = 0;
    for (auto& vec : mAccesses) {
      max = (vec.size() > max) ? vec.size() : max;
    }
    std::stringstream ss;
    ss << "\t";
    for (auto i = 1; i <= max; i++) {
      ss << i << "\t";
    }
    ss << std::endl;
    for (auto i = 0; i < mAccesses.size(); i++) {
      auto& vec = mAccesses[i];
      ss << get_iterator_for_depth(i + 1) << "\t";
      for (auto j = 0; j < max; j++) {
        if (j >= vec.size()) {
          ss << "0\t";
        } else {
          ss << vec[j] << "\t";
        }
      }
      ss << std::endl;
    }
    return ss.str();
  }

  std::string get_iterator_for_depth(size_t depth) const {
    if (depth == get_depth()) {
      return mIterator->get_name().getString();
    } else {
      return mParent->get_iterator_for_depth(depth);
    }
  }

  static int kUidCtr;
};

int LoopCtx::kUidCtr = 0;

std::ostream& operator<<(std::ostream &strm, const LoopCtx &ctx) {
  return strm << ctx.to_string();
}

class RoseVisitor : public ROSE_VisitorPattern
{
public:
  RoseVisitor( ) { }

  void visit( SgFunctionDefinition *fnDef ) {
    // Grab the function's declaration
    SgFunctionDeclaration *fnDecl = fnDef->get_declaration();
    // Find all of the for loops in the function's body
    auto loops = NodeQuery::querySubTree(fnDef->get_body(), V_SgForStatement);
    // For each loop found:
    for (auto node : loops) {
      // Search for an enclosing loop
      auto encl = SageInterface::findEnclosingLoop((SgStatement*)node->get_parent());
      // If one is not found
      if (!encl) {
        // Assume it is a top-level loop and visit the loop nest
        this->visit((SgForStatement*)node);
      }
    }
  }

  void visit( SgForStatement *stmt, LoopCtx *parent = nullptr ) {
    LoopCtx ctx(stmt, parent);
    auto body = ctx.get_loop_body()->get_statements();
    auto any = false, noNesting = true;
    for (SgStatement *stmt : body) {
      if (isSgForStatement(stmt)) {
        this->visit((SgForStatement*)stmt, &ctx); noNesting = false;
      } else {
        auto arrRefs = NodeQuery::querySubTree(stmt, V_SgPntrArrRefExp);
        for (auto node : arrRefs) {
          auto arrRef = isSgPntrArrRefExp(node);
          if (!isSgPntrArrRefExp(arrRef->get_lhs_operand())) {
            ctx.add_arr_ref(find_top_arr_ref(arrRef));
          }
        }
      }
    }
    std::cout << "START: " << ctx << std::endl;
    // std::cout << "RANGE: [" << ctx.get_lower_bound()->unparseToString() << "," << ctx.get_upper_bound()->unparseToString() << "]" << std::endl;
    std::cout << "RANGE: [" << ctx.get_lower_bound()->unparseToString() << ":" << ctx.get_stride()->unparseToString() << ":" << ctx.get_upper_bound()->unparseToString() << "]" << std::endl;
    if (ctx.has_arr_refs()) {
      for (auto arrRef : ctx.get_arr_refs()) {
        if (std::get<0>(arrRef) >= ctx.get_depth()) {
          visit(std::get<1>(arrRef), &ctx);
        }
      }
      if (noNesting) {
        std::cout << "ACCESS SUMMARY:" << std::endl << ctx.access_matrix();
      }
    }
    if (noNesting) {
      std::cout << "DEPTH: " << ctx.get_depth() << std::endl;
    }
    std::cout << "END: " << ctx << std::endl;
  }

  size_t find_arr_ref_depth( SgPntrArrRefExp* exp ) {
    auto depth = 0;
    auto next  = exp;
    while (next) { 
      depth += 1;
      next   = isSgPntrArrRefExp(next->get_lhs_operand());
    }
    return depth;
  }

  SgPntrArrRefExp* find_top_arr_ref( SgPntrArrRefExp* exp ) {
    auto next = exp->get_parent();
    SgPntrArrRefExp* top = exp;
    while (!isSgStatement(next)) {
      if (isSgPntrArrRefExp(next)) {
        top = (SgPntrArrRefExp*)next;
      } else if (top) {
        break;
      }
      next = next->get_parent();
    }
    return top;
  }

  void visit( SgPntrArrRefExp *exp, LoopCtx *ctx, size_t depth = 1) {
    auto lhs = exp->get_lhs_operand();
    if (isSgPntrArrRefExp(lhs)) {
      visit((SgPntrArrRefExp*)lhs, ctx, depth + 1);
    }
    std::vector<SgVarRefExp*> refs;
    SageInterface::collectVarRefs(exp->get_rhs_operand(), refs);
    for (auto ref : refs) {
      auto iterDepth = ctx->find_iterator_depth(ref->get_symbol());
      if (iterDepth) {
        ctx->access(iterDepth, depth);
        if (iterDepth == ctx->get_depth()) {
          std::cout << "ACCESS: " << find_top_arr_ref(exp)->unparseToString() << std::endl;
        }
      }
    }
  }
};

using handle_t   = AbstractHandle::abstract_handle;
using location_t = std::tuple<char*, int>;

SgFile* get_file_by_name(SgProject* project, const char* name) {
  auto slash = strrchr(name, '/');
  if (slash) {
    name = slash + 1;
  }
  for (auto file : project->get_files()) {
    auto path = file->getFileName();
    auto pos  = path.length() - strlen(name);
    if (!strcmp(name, path.c_str() + pos)) {
      return file;
    }
  }
  return nullptr;
}

int main (int argc, char* argv[]) {
  std::vector<location_t> locations;
  for (auto i = 0; i < argc; i++) {
    auto colon = strrchr(argv[i], ':');
    if (colon) {
      *colon = '\0';
      locations.push_back(std::make_tuple(argv[i], atoi(colon + 1)));
    }
  }

  SgProject* project = frontend( argc, argv );
  ROSE_ASSERT( project );

  SageInterface::changeAllBodiesToBlocks( project );
  RoseVisitor visitor;

  if (!locations.size()) {
    traverseMemoryPoolVisitorPattern( visitor );
  }

  for (auto location : locations) {
    auto file = get_file_by_name( project, std::get<0>(location) );
    ROSE_ASSERT( file );
    auto fNode   = AbstractHandle::buildroseNode(isSgSourceFile(file));
    auto fHandle = new handle_t(fNode);
    handle_t* sHandle = new handle_t(fHandle, "SgNode<position," + std::to_string(std::get<1>(location)) + ">");
    ROSE_ASSERT( sHandle && sHandle->getNode() && "unable to locate statement" );
    auto sNode = (SgNode *)(sHandle->getNode()->getNode());
    SgScopeStatement *encl, *next;
    if (isSgForStatement(sNode)) {
      next = (SgScopeStatement *)sNode;
    } else {
      next = SageInterface::findEnclosingLoop((SgStatement *)sNode);
    }
    do {
      encl = next;
      next = SageInterface::findEnclosingLoop((SgStatement *)encl->get_parent());
    } while (encl != next && next);
    ROSE_ASSERT( encl && "unable to identify enclosing loop" );
    visitor.visit((SgForStatement*)encl);
  }

  return 0;
}

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re

from statistics import mean
from sys        import stdin
from sympy      import simplify
from sympy.parsing.sympy_parser import parse_expr

cache = dict()
access_ptn = re.compile(r"(ACCESS): (.*)")
ident_ptn  = re.compile(r"[a-zA-Z_][a-zA-Z0-9_]*")
ctx_ptn    = re.compile(r"(START): LoopCtx\((.*?),.*\)#(?:\d+)")
range_ptn  = re.compile(r"(RANGE): \[(.*?),\s*(.*?)\]")
line_len   = 8

class Cache:
    def __init__(self):
        self.hit = set()
        self.all = set() 

    def access(self, idx):
        if idx not in self.all:
            for i in range(line_len):
                self.all.add(idx + i)
        self.hit.add(idx)

def access(lhs, idx):
    if lhs not in cache:
        cache[lhs] = Cache()
    cache[lhs].access(idx)

def compute_reuse():
    scores = []
    for _, val in cache.items():
        score = 1 - len(val.all.difference(val.hit)) / len(val.all)
        scores.append(score)
    print(mean(scores))

def find_locals(s):
    return { key : line_len for key in ident_ptn.findall(s) }

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches

def build_lhs_and_rhs(expr, itr, val):
    expr = g[1]
    idxs = list(find_all(expr, "["))
    lhs  = expr[:idxs[0]]
    for idx in idxs[:-1]:
        start, end = idx + 1, expr.find("]", idx)
        env = find_locals(expr[start:end])
        if itr in env:
            env[itr] = val
            lhs += "[{0}]".format(eval(expr[start:end], None, env))
        else:
            lhs += "[{0}]".format(expr[start:end])
    rhs = expr[(idxs[-1] + 1):-1]
    env = find_locals(rhs)
    env[itr] = val
    rhs = eval(rhs, None, env)
    return lhs, rhs

def format_range(start, end):
    try:
        return str(simplify(parse_expr(end + " - " + start + " + 1")))
    except:
        return None

last_ctx = None
limits   = list()
for line in stdin:
    m = ctx_ptn.search(line) or access_ptn.search(line) or range_ptn.search(line)
    g = m.groups() if m else ("NONE")
    if g[0] == "START":
        last_ctx = g[1]
    elif g[0] == "ACCESS":
        for i in range(line_len):
            access(*build_lhs_and_rhs(g[1], last_ctx, i))
    elif g[0] == "RANGE":
        limits.append(format_range(*g[1:]))

if cache:
    compute_reuse()

if limits:
    if all(limits):
        print("[%s]" % ", ".join(limits))
    else:
        print("#N/A")

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import re
import operator
import itertools
import subprocess

from functools  import reduce 
from statistics import mean
from sys        import stdin, argv
from io         import TextIOWrapper
from sympy      import simplify
from sympy.parsing.sympy_parser import parse_expr

main_path = os.path.dirname(os.path.realpath(__file__))
main_exe  = os.path.join(main_path, "main")
perm_exe  = os.path.join(main_path, "../permute/main")

depth_ptn  = re.compile(r"depth=(\d+),location=.*?")
access_ptn = re.compile(r"(ACCESS): (.*)")
ident_ptn  = re.compile(r"[a-zA-Z_][a-zA-Z0-9_]*")
start_ptn  = re.compile(r"(START): LoopCtx\((.*?),(.*?),.*\)#(\d+)")
end_ptn    = re.compile(r"(END): LoopCtx\(.*?,(.*?),.*\)#(?:\d+)")
range_ptn  = re.compile(r"(RANGE): \[(.*?),\s*(.*?)\]")
mat_ptn    = re.compile(r"(.*?)\t([\d\t]+)")
line_len   = 8
cutoff     = 16

class Cache:
    def __init__(self):
        self.hit = set()
        self.all = set() 

    def access(self, idx):
        if idx not in self.all:
            for i in range(line_len):
                self.all.add(idx + i)
        self.hit.add(idx)

def access(lhs, idx):
    if lhs not in cache:
        cache[lhs] = Cache()
    cache[lhs].access(idx)

def compute_reuse():
    scores = []
    for _, val in cache.items():
        score = 1 - len(val.all.difference(val.hit)) / len(val.all)
        scores.append(score)
    return mean(scores)

def find_locals(s):
    return { key : line_len for key in ident_ptn.findall(s) }

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches

def build_lhs_and_rhs(expr, itr, val):
    expr = g[1]
    idxs = list(find_all(expr, "["))
    lhs  = expr[:idxs[0]]
    for idx in idxs[:-1]:
        start, end = idx + 1, expr.find("]", idx)
        env = find_locals(expr[start:end])
        if itr in env:
            env[itr] = val
            lhs += "[{0}]".format(eval(expr[start:end], None, env))
        else:
            lhs += "[{0}]".format(expr[start:end])
    rhs = expr[(idxs[-1] + 1):-1]
    env = find_locals(rhs)
    env[itr] = val
    rhs = eval(rhs, None, env)
    return lhs, rhs

def format_range(start, end):
    try:
        return str(simplify(parse_expr(end + " - " + start + " + 1")))
    except:
        return None

def matches(perm, ptn):
    for x, y in zip(perm, ptn):
        if y != "*" and str(x) != y:
            return False
    return True

info = dict()
permutables = None
with subprocess.Popen([ perm_exe ] + argv[1:], stdout=subprocess.PIPE) as proc:
    for line in TextIOWrapper(proc.stdout, encoding="utf-8"):
        if line.startswith('LOOP'):
            lid = line[5:line.index(':')]
            info[lid] = [ x.strip() for x in line[line.index(':') + 2:].split(",") ]
        elif line.startswith('PERMUTABLE: '):
            permutables = line[12:].strip()
if not permutables:
    exit(-1)

print('PERMUTABLE:', permutables)

permutables=permutables.split('),')
permutations=[]
for permutable in permutables:
    if '(' in permutable:
        permutable=permutable.split('(',1)[1]
        permutable=permutable.split(',')
        permutable=[int(v) for v in permutable]
        cnt_permutations=list(itertools.permutations(permutable))
        if not permutations:
            permutations=cnt_permutations
        else:
            permutations=itertools.product(cnt_permutations)

# filter out trivial permutations
permutations = [ perm for perm in permutations if not all(x<y for x, y in zip(perm, perm[1:])) ]

perms = { perm : 0 for perm in permutations }
def grade_permutations(ptns, weight=1):
    for ptn in ptns:
        ptn = tuple(ptn[1:-1].split(","))
        for perm in perms:
            if matches(perm, ptn):
                perms[perm] += weight

last_ctx = None
scores   = dict()
limits   = dict()
loops    = dict()
accesses = dict()
saving   = False
with subprocess.Popen([ main_exe ] + argv[1:], stdout=subprocess.PIPE) as proc:
    for line in TextIOWrapper(proc.stdout, encoding="utf-8"):
        if saving:
            m = mat_ptn.search(line)
            g = m.groups() if m is not None else None
            if g is not None:
                if g[0]:
                    accesses[g[0]] = [ int(x) for x in g[1].split("\t") if x ]
                continue
            else:
                saving = False
        m = start_ptn.search(line) or end_ptn.search(line) or \
            access_ptn.search(line) or range_ptn.search(line)
        g = m.groups() if m else ("NONE")
        if g[0] == "START":
            cache = dict()
            # info[g[3]] += [ "location={}".format(g[1]) ]
            loops[g[2]] = g[3]
            last_ctx = g[2]
        elif g[0] == "END":
            if cache:
                scores[last_ctx] = compute_reuse()
            last_ctx = None
        elif g[0] == "ACCESS":
            for i in range(line_len):
                access(*build_lhs_and_rhs(g[1], last_ctx, i))
        elif g[0] == "RANGE":
            limits[last_ctx] = format_range(*g[1:])
        elif "ACCESS SUMMARY:" in line:
            saving = True
    os.waitpid(proc.pid, 0)

def format_ordering(*argv):
    argv = list(argv)
    argv = (['*'] * (len(loops) - len(argv))) + argv
    return "({})".format(",".join(argv))

recommendations = []

if scores:
    scores = { loops[x]: y for x, y in scores.items() }
    for loop, score in scores.items():
        info[loop] += [ "clu_score={}".format(score) ]
    max_score = max(scores.values())
    max_loops = [ x for x, y in scores.items() if y == max_score ]
    if len(max_loops) == 1:
        recommendations.append("CLU RECOMMENDATIONS: {}".format(format_ordering(max_loops[0])))
        grade_permutations([ format_ordering(max_loops[0]) ])

if limits:
    known = { loops[x] : (int(y) < cutoff) for x, y in limits.items() if y.isnumeric() }
    if any(known.values()):
        orderings = [ format_ordering(loop) for loop in known.keys() if known[loop] ]
        recommendations.append("LIMITS RECOMENDATIONS: {}".format(",".join(orderings)))
        grade_permutations(orderings)
    if all(limits.values()):
        for loop, limit in limits.items():
            info[loops[loop]] += [ "num_iters={}".format(limit) ]
    else:
        print("#N/A")

def prune_accesses(accesses):
    for l in accesses.keys():
        if len(accesses[l]) > 1:
            accesses[l][1] += accesses[l][0]
            accesses[l].pop(0)

def access_ordering(accesses,curr=None):
    to_place = list(accesses.keys())
    ordering = []
    while len(to_place) > 1:
        if not curr:
            while all(accesses[x][0] == 0 for x in accesses.keys()):
                prune_accesses(accesses)
            curr = max(accesses.keys(),key=lambda loop: accesses[loop][0])
            dups = [ x for x in accesses.keys() if x != curr and accesses[x][0] == accesses[curr][0] ]
            if any(dups):
                sub_orderings = reduce(operator.add, \
                    (access_ordering(dict(accesses),x) for x in (dups + [curr])))
                return [ ordering + x for x in sub_orderings ]
        ordering.append(loops[curr])
        to_place.remove(curr)
        del accesses[curr]
        prune_accesses(accesses)
        curr = None
    ordering.append(loops[accesses.popitem()[0]])
    return [ ordering ]

if accesses:
    orderings = [ format_ordering(*reversed(x)) for x in reversed(access_ordering(accesses)) ]
    recommendations.append("ACCESS RECOMENDATIONS: {}".format(",".join(orderings)))
    grade_permutations(orderings)

if info:
    for loop, values in info.items():
        print("LOOP {}: {}".format(loop, ",".join(values)))

if recommendations:
    print("\n".join(recommendations))

if perms:
    print("PERMUTATIONS:",list(sorted(perms.keys(), reverse=True, key=lambda x : perms[x])))

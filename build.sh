#!/bin/bash

# exit when any command fails
set -e

if [[ -z "${ROSE_HOME}" ]] || [[ ! -d "${ROSE_HOME}" ]]; then
  echo "fatal> ROSE_HOME incorrectly set, please verify it exists then try again." 1>&2
  exit 1
fi

if [[ -z "${JAVA_HOME}" ]] || [[ ! -d "${JAVA_HOME}" ]]; then
  echo "warn> JAVA_HOME incorrectly set, build may fail if your ROSE installation requires the JDK." 1>&2
fi

echo "info> building the rhs analysis utility"
cd rhsAnalysis/
make

echo "info> building the (primary) loop utility"
cd ../loopUtil/
make

echo "info> building the loop permutation utility"
cd ../permute/
make


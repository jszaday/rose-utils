#include <iostream>
#include <string>

#define NUM_MESSAGES 10

std::string getMessage(int j)
{
  std::string message("Hello, world from #");
  message += (j + '0');
  message += "!";
  return (message);
}

float testFloat(float x, float y) {
  float z = (x * x) + (y * y);
  return z * z;
}

int weird(int n) {
  int acc = 0;
  for (int i = 1; i < n; i++) {
    acc += weird(i - 1) + i;
  }
  acc = 10;
  return acc;
}

int main()
{
  for (int i = 0; i < weird(4); i++) {
    std::cout << getMessage(i) << std::endl;
  }

  return 0;
}

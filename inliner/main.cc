#include <rose.h>
#include <iostream>
#define DEBUG

Sg_File_Info *kFileInfo;
SgNode       *kRootNode;

/** TODO:
  * - Check for Void Return type
  * - Check for Recursion
  */

class ClassicVisitor : public ROSE_VisitorPattern {
public:
  void visit(SgFunctionDefinition *functionDefinition) {
    // Grab the function's declaration
    SgFunctionDeclaration *functionDeclaration = functionDefinition->get_declaration();
    // And its body (which we converted to a block in Main)
    SgBasicBlock          *body                = functionDefinition->get_body();
    /**
     * Ensure that the function is actually contained in the file we're testing
     * If it's not... then return ('cause we don't wanna' process it)
     */
    if (!kFileInfo->isSameFile(functionDeclaration->get_file_info())) return;
#ifdef DEBUG
    std::cout << "Found a function node named " << functionDeclaration->get_name().getString() << "." << std::endl;
#endif
    // Next, query the function's body for all of the 'for' loops it contains
    Rose_STL_Container<SgNode*> loopList = NodeQuery::querySubTree(body, V_SgForStatement);
    // And begin iterating through them
    for (Rose_STL_Container<SgNode*>::iterator lItr = loopList.begin(); lItr != loopList.end(); ++lItr) {
      // Converting each one to an SgForStatement
      SgForStatement *currLoop = isSgForStatement(*lItr);
      // Normalizing it
      SageInterface::forLoopNormalization(currLoop);
      // Then visiting it
      this->visit(functionDefinition, currLoop);
    }
  }

  void visit(SgFunctionDefinition* functionDefinition, SgForStatement* forStatement) {
    SgBasicBlock *body = SageInterface::ensureBasicBlockAsBodyOfFor(forStatement);

    ROSE_ASSERT(body);

    Rose_STL_Container<SgNode*> funList = NodeQuery::querySubTree(body, V_SgFunctionCallExp);
    // And begin iterating through them
    for (Rose_STL_Container<SgNode*>::iterator flItr = funList.begin(); flItr != funList.end(); ++flItr) {
      // Converting each one to an SgFunctionCallExp
      SgFunctionCallExp *currCall = isSgFunctionCallExp(*flItr);
      SgFunctionRefExp  *refExp   = isSgFunctionRefExp(currCall->get_function());

      if (refExp == NULL) continue;

      std::string targetName = refExp->get_symbol_i()->get_name().getString();
      SgFunctionDeclaration* functionDeclaration = SageInterface::findFunctionDeclaration(kRootNode, targetName, NULL, true);

      if (functionDeclaration == NULL
          || !kFileInfo->isSameFile(functionDeclaration->get_file_info())) continue;

      SgFunctionDefinition     *functionDefinition = functionDeclaration->get_definition();
      SgInitializedNamePtrList &params             = functionDeclaration->get_args();
      SgBasicBlock             *funBody            = SageInterface::deepCopy(functionDefinition->get_body());

      // Get the list of actual argument expressions from the function call, which we'll later use to initialize new local
      // variables in the inlined code.  We need to detach the actual arguments from the AST here since we'll be reattaching
      // them below (otherwise we would violate the invariant that the AST is a tree).
      SgExpressionPtrList funargs = currCall->get_args()->get_expressions();
      currCall->get_args()->get_expressions().clear();
      // For each of the arguments...
      SgInitializedNamePtrList::iterator formalIter = params.begin();
      SgExpressionPtrList::iterator      actualIter = funargs.begin();
      for (size_t argNumber = 0;
           formalIter != params.end() && actualIter != funargs.end();
           ++argNumber, ++formalIter, ++actualIter) {
         SgInitializedName *formalArg = *formalIter;
         SgExpression      *actualArg = *actualIter;
         // Generate a new name for the argument in the format of __<OLD>_<GIBBERISH>__
         std::string newName = SageInterface::generateUniqueVariableName(body, formalArg->get_name().getString() + std::string("_"));
         SgName sgNewName(newName);
         // Create an initializer for the assignment - ensuring that its end of construct is correct
         SgAssignInitializer   *initializer = new SgAssignInitializer(kFileInfo, actualArg, formalArg->get_type());
         initializer->set_endOfConstruct(kFileInfo);
         // Then create the declaration itself using the initializer
         SgVariableDeclaration *varDecl     = new SgVariableDeclaration(kFileInfo, sgNewName, formalArg->get_type(), initializer);
         // Add the declaration to the function's body
         SageInterface::prependStatement(varDecl, funBody);
         // Find the formal argument's symbol in the function definition
         SgVariableSymbol *oldSym = SageInterface::lookupVariableSymbolInParentScopes(formalArg->get_name(), functionDefinition);
         // And just grab the first symbol for our new variable
         SgVariableSymbol *newSym = SageInterface::getFirstVarSym(varDecl);
         // Assert that we sucessfully got both
         ROSE_ASSERT(oldSym && newSym);
         // Then, replace all of the references to the old symbol with the new one
         SageInterface::replaceVariableReferences(oldSym, newSym, funBody);
      }
      SgStatement* parent;
      SgNode* tmp = currCall;

      while ((parent = isSgStatement(tmp = tmp->get_parent())) == NULL && tmp != NULL);
      ROSE_ASSERT(parent);

      SgName resultName;
      resultName << SageInterface::generateUniqueVariableName(body, std::string("result_"));
      SgName endName;
      endName << SageInterface::generateUniqueVariableName(body, std::string("end_"));

      SgVarRefExp *resultRef = SageBuilder::buildVarRefExp(resultName.getString(), funBody);
      SgLabelStatement *endOfInline = SageBuilder::buildLabelStatement(endName, NULL, functionDefinition);

      SgVariableDeclaration *resultDecl = new SgVariableDeclaration(kFileInfo, resultName, functionDeclaration->get_orig_return_type());;
      SageInterface::insertStatement(parent, resultDecl);

      // Insert the body of the function we're inlining before the statement
      SageInterface::insertStatement(parent, funBody);

      SageInterface::insertStatement(parent, endOfInline);

      SgSymbolTable* symbolTable    = funBody->get_symbol_table();
      SgLabelSymbol* endOfInlineSym = new SgLabelSymbol(endOfInline);
      endOfInlineSym->set_parent(symbolTable);
      symbolTable->insert(endOfInline->get_name(), endOfInlineSym);

      Rose_STL_Container<SgNode*> retList = NodeQuery::querySubTree(funBody, V_SgReturnStmt);
      for (Rose_STL_Container<SgNode*>::iterator rlItr = retList.begin(); rlItr != retList.end(); ++rlItr) {
        SgReturnStmt    *currRet = isSgReturnStmt(*rlItr);
        SgExprStatement *assn    = SageBuilder::buildAssignStatement(resultRef, currRet->get_expression());
        SgGotoStatement *gotoEnd = SageBuilder::buildGotoStatement(endOfInlineSym);
        SageInterface::replaceStatement(currRet, assn);
        SageInterface::insertStatementAfter(assn, gotoEnd);
      }

      SageInterface::replaceExpression(currCall, resultRef, true);

      bool sucessfullyInlined = true;
      if (sucessfullyInlined) {
        std::cout << "Call to " << targetName << " was sucessfully inlined." << std::endl;
      } else {
        std::cout << "Call to " << targetName << " was NOT sucessfully inlined." << std::endl;
      }
    }
  }
};

int main (int argc, char* argv[]) {
  SgProject* project = frontend(argc, argv);
  ROSE_ASSERT( project != NULL );

  kFileInfo = project->get_file(0).get_file_info();
  kRootNode = project;

  SageInterface::changeAllBodiesToBlocks( project );

  ClassicVisitor visitor;
  traverseMemoryPoolVisitorPattern( visitor );

  return backend( project );
}

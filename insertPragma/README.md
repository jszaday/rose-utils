Program tries to find the closest and/or enclosing loop near line number passed as argument.

Usage is: ./insertPragma <file:line> <pragma text>

For example: ./insertPragma test.cc:3 "uiuc_interchange 1,2,0"

Adds pragma "#pragma uiuc_interchange 1,2,0" to line 3 of test.cc, output will be in rose_test.cc.

The binary loopInterchange is Thiago's tool, the file interchange is a bash script that automatically inserts a pragma and calls Thiago's tool on the resulting program. Unlike his tool, for convenience, loop nest level numbering starts at 1. Example usage is: ./interchage test.cc:3 1,3,2

#include <rose.h>
#include <iostream>
#include <fstream>
#include <ctype.h>

using handle_t = AbstractHandle::abstract_handle;
using location_t = std::tuple<char *, int>;

SgFile *get_file_by_name(SgProject *project, const char *name) {
  auto slash = strrchr(name, '/');
  if (slash) {
    name = slash + 1;
  }
  for (auto file : project->get_files()) {
    auto path = file->getFileName();
    auto pos = path.length() - strlen(name);
    if (!strcmp(name, path.c_str() + pos)) {
      return file;
    }
  }
  return nullptr;
}

int main(int argc, char *argv[]) {
  std::vector<location_t> locations;
  int i;
  for (i = 1; i < argc; i++) {
    auto colon = strrchr(argv[i], ':');
    if (!colon) {
      break;
    }
    *colon = '\0';
    auto *it = colon + 1;
    while (*it && isdigit(*it))
      it++;
    *it = '\0';
    locations.push_back(std::make_tuple(argv[i], atoi(colon + 1)));
  }
  ROSE_ASSERT(i < argc);
  auto pragmaOffset = i;
  std::vector<const char *> args(argv, argv + (argc - pragmaOffset + 1));
  args.push_back("-rose:skipfinalCompileStep");
  SgProject *project = frontend(args.size(), const_cast<char **>(args.data()));
  ROSE_ASSERT(project);

  SageInterface::changeAllBodiesToBlocks(project);

  for (auto location : locations) {
    auto name = std::get<0>(location);
    auto file = get_file_by_name(project, name);
    auto fNode = AbstractHandle::buildroseNode(isSgSourceFile(file));
    auto fHandle = new handle_t(fNode);
    handle_t *sHandle =
        new handle_t(fHandle, "SgStatement<position," +
                                  std::to_string(std::get<1>(location)) + ">");
    ROSE_ASSERT(sHandle && sHandle->getNode() && "unable to locate statement");
    auto sNode = (SgNode *)(sHandle->getNode()->getNode());
    SgScopeStatement *encl, *next;
    if (isSgForStatement(sNode)) {
      next = (SgScopeStatement *)sNode;
    } else {
      next = SageInterface::findEnclosingLoop((SgStatement *)sNode);
    }
    do {
      encl = next;
      next = SageInterface::findEnclosingLoop((SgStatement *)encl->get_parent());
    } while (encl != next && next);
    ROSE_ASSERT(encl && "unable to identify enclosing loop");
    auto pragma = SageBuilder::buildPragmaDeclaration(argv[pragmaOffset], encl);
    SageInterface::insertStatementBefore(encl, pragma);
  }

  return backend(project);
}

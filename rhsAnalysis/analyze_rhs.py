from shutil import copyfile
from functools import reduce
from subprocess import Popen, PIPE
from operator import itemgetter

from sympy import simplify
from sympy.parsing.sympy_parser import parse_expr

import re, os, sys
import operator

curr_path = os.path.dirname(os.path.realpath(__file__))
main_file = os.path.join(curr_path, "main")

def find_difference(pair):
    try:
        return str(simplify(parse_expr(pair[1] + " - " + pair[0])))
    except:
        print("WARNING: could not check %s and %s." % pair, file=sys.stderr)
        return None

def is_integer(x):
    try:
        x = int(x)
        return True
    except:
        return False

def analyze_rhs(src_path, suppress=False):
    process = Popen([main_file, src_path], stdout=PIPE)
    out, err = process.communicate()
    if out and not err:
        out = out.decode('utf-8')
        if not suppress:
            print(out, file=sys.stderr)
        rhs_count = sum(map(int, re.findall(r"RHS OP COUNT: (\d+)", out)))
        offsets = set(filter(bool, map(find_difference, re.findall(r"CHECK: \((.*?)\) AND \((.*?)\)", out))))
        recurrence = any(int(x) < 0 for x in offsets if is_integer(x))
        init_only = bool(reduce(operator.and_, map(int, re.findall(r"INIT ONLY: (0|1)", out)), 1))
        reduction = bool(reduce(operator.or_, map(int, re.findall(r"REDUCTION: (0|1)", out)), 0))
        return (rhs_count, init_only, reduction, recurrence, offsets)
    else:
        return ("", "")
#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
import lore
import argparse
import operator
import os, re, sys

from shutil import copyfile
from functools import reduce
from subprocess import Popen, PIPE
from operator import itemgetter

from sympy import simplify
from sympy.parsing.sympy_parser import parse_expr

from analyze_rhs import analyze_rhs

args   = ("gienah.cs.illinois.edu", "gong15", "polaris", "vectorization")
cursor = lore.connect(args)
mtn_cache = {}
loop_collections = "/shared/loop_collections_12072018"

parser = argparse.ArgumentParser(description='Find symptoms present in a batch of loops.')
parser.add_argument('loops', metavar='loop', type=str, nargs='*', help='loops to process')
parser.add_argument("-i", nargs="*", dest="files", help="list of input files")
parser.add_argument('--copy', action='store_true', help='copy src files to cur dir')
args = parser.parse_args()

def read_loop_file(lfile):
    with open(lfile) as f:
        return list(map(fetch_loop_id, filter(bool, map(str.strip, f.readlines()))))

mtn_ptn = re.compile(r"(.*\.c)_(.*)_line(\d+)_(\d+)")
name_ptn = re.compile(r"(.*\.c)_(.*)_line(\d+)")
def fetch_loop_id(name):
    if ":" in name:
        name = name[(name.index(":") + 1):].strip()
    m = mtn_ptn.search(name) or name_ptn.search(name)
    m = m.groups() if m else None
    if m is None:
        return name
    query =  """SELECT `table_ptr` FROM loops
                WHERE `file`=%s AND `function`=%s AND `line`=%s
                LIMIT 1"""
    _, lid = cursor.execute(query, m[:3]), cursor.fetchone()[0]
    if len(m) == 4:
        mtn_cache[lid] = [ m[3] ]
    else:
        mtn_cache[lid] = [ "0" ]
    return lid

def fetch_loop_info(lid):
    query = '''SELECT `benchmark`, `version`, `application`, `file`, `function`, `line`
               FROM loops WHERE `table_ptr`=%s'''
    cursor.execute(query, (lid,))
    return cursor.fetchone()

loops = set(map(fetch_loop_id, args.loops))
if args.files:
    loops = loops.union(set(reduce(list.__add__, map(read_loop_file, args.files))))

if not len(loops):
    parser.parse_args(["-h"])

print("name,rhs_op_count,init_only,scalar_reduction,recurrence,offsets")
for lid in loops:
    for mid in mtn_cache.get(lid, ['0']):
        info = fetch_loop_info(lid)
        if not info:
            print("WARNING: no info for:", lid, file=sys.stderr)
            continue
        name = "_".join(list(info[3:-1]) + ["line" + str(info[-1])])
        loop_path = os.path.join(*([loop_collections] + list(info[:3]) + ["extractedLoops", name + "_loop"]))
        src_path  = os.path.join(*[loop_path + ".c_mutations", name + "_loop.c." + str(mid) + ".c"])
        results = analyze_rhs(src_path)
        print("%s_%s" % (name, mid), *results[:-1], '"%s"' % list(results[-1]), sep=",")
        if args.copy:
            copyfile(src_path, os.path.join(".", os.path.basename(src_path)))

lore.disconnect()

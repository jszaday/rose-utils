#include <rose.h>
#include <iostream>
#include <fstream>
#include <set>
#include <utility>

class LoopCtx
{
  LoopCtx *mParent;
  SgForStatement *mStmt;
  SgVariableSymbol *mIterator;
  SgExpression *mLowerBound;
  SgExpression *mUpperBound;
  SgExpression *mStride;
  int mUid;
public:
  LoopCtx( SgForStatement* stmt, LoopCtx* parent = nullptr )
  : mStmt(stmt), mParent(parent) {
    SageInterface::forLoopNormalization(mStmt);
    ROSE_ASSERT(SageInterface::isCanonicalForLoop(mStmt));
    SageInterface::getForLoopInformations(mStmt, mIterator, mLowerBound, mUpperBound, mStride);
    mUid = ++kUidCtr;
  }
  
  SgBasicBlock* get_loop_body()   { return isSgBasicBlock(mStmt->get_loop_body()); }

  std::string get_iterator_string() const {
    return mIterator->get_name().getString();
  }

  std::string get_location() const {
    auto fNode = AbstractHandle::buildroseNode(mStmt);
    auto fName = fNode->getFileName();
    auto idx   = fName.find_last_of("/");
    return fName.substr(idx + 1, std::string::npos) + ":" + std::to_string(fNode->getStartPos().line);
  }

  std::string to_string() const {
    std::stringstream ss;
    ss << "LoopCtx(" ;
    ss << get_location() << ",";
    ss << get_iterator_string() << ",";
    if (mParent) {
      ss << mParent->to_string();
    } else {
      ss << "NULL";
    }
    ss << ")#" << mUid;
    return ss.str();
  }

  static int kUidCtr;
};

int LoopCtx::kUidCtr = 0;

std::ostream& operator<<(std::ostream &strm, const LoopCtx &ctx) {
  return strm << ctx.to_string();
}

class RoseVisitor : public ROSE_VisitorPattern
{
public:
  RoseVisitor( ) { }

  void visit( SgFunctionDefinition *fnDef ) {
    // Grab the function's declaration
    SgFunctionDeclaration *fnDecl = fnDef->get_declaration();
    // Find all of the for loops in the function's body
    auto loops = NodeQuery::querySubTree(fnDef->get_body(), V_SgForStatement);
    // For each loop found:
    for (auto node : loops) {
      // Search for an enclosing loop
      auto encl = SageInterface::findEnclosingLoop((SgStatement*)node->get_parent());
      // If one is not found
      if (!encl) {
        // Assume it is a top-level loop and visit the loop nest
        this->visit((SgForStatement*)node);
      }
    }
  }

  void visit( SgForStatement *stmt, LoopCtx *parent = nullptr ) {
    LoopCtx ctx(stmt, parent);
    auto body = ctx.get_loop_body()->get_statements();
    bool initOnly = true, empty = true, reduction = false;
    std::set<std::pair<std::string, std::string>> toCheck;
    int rhsOps = 0;
    for (SgStatement *stmt : body) {
      if (isSgForStatement(stmt)) {
        this->visit((SgForStatement*)stmt, &ctx);
      } else if (isSgExprStatement(stmt)) {
        auto expr = ((SgExprStatement*)stmt)->get_expression();
        if (isSgAssignOp(expr) || isSgCompoundAssignOp(expr)) {
          auto lhs = ((SgBinaryOp*)expr)->get_lhs_operand();
          auto rhs = ((SgBinaryOp*)expr)->get_rhs_operand();
          if (isSgVarRefExp(lhs)) {
            auto symbol = ((SgVarRefExp*)lhs)->get_symbol();
            auto others = SageInterface::getSymbolsUsedInExpression(rhs);
            if (isSgCompoundAssignOp(expr) ||
                std::find(others.begin(), others.end(), symbol) != others.end()) {
              reduction = true;
            }
          }
          rhsOps += visit_rhs(rhs) + (isSgCompoundAssignOp(expr) ? 1 : 0);
          auto arrRefs = NodeQuery::querySubTree(rhs, V_SgPntrArrRefExp);
          initOnly = initOnly && (arrRefs.size() == 0); empty = false;
          if (isSgPntrArrRefExp(lhs)) {
            auto getLhsStr = [](SgNode* ptr) {
              return ((SgPntrArrRefExp*)ptr)->get_lhs_operand()->unparseToString();
            };
            auto getRhsStr = [](SgNode* ptr) {
              return ((SgPntrArrRefExp*)ptr)->get_rhs_operand()->unparseToString();
            };
            if (isSgCompoundAssignOp(expr)) {
              arrRefs.push_back(lhs);
            }
            for (auto other : arrRefs) {
              if (getLhsStr(lhs) == getLhsStr(other)) {
                toCheck.insert(std::make_pair(getRhsStr(lhs), getRhsStr(other)));
              }
            }
          }
        }
      }
    }
    std::cout << "START: " << ctx << std::endl;
    std::cout << "RHS OP COUNT: " << rhsOps << std::endl;
    if (!empty) {
      std::cout << "REDUCTION: " << reduction << std::endl;
      std::cout << "INIT ONLY: " << (initOnly && !reduction) << std::endl;
      for (auto pair : toCheck) {
        std::cout << "CHECK: (" << pair.first << ") AND (" << pair.second << ")" << std::endl;
      }
    }
    std::cout << "END: " << ctx << std::endl;
  }

  int visit_rhs(SgExpression* rhs) {
    if (isSgLshiftOp(rhs) || isSgRshiftOp(rhs) || 
        isSgBitAndOp(rhs) || isSgBitOrOp(rhs) || isSgBitXorOp(rhs) ||
        isSgAddOp(rhs) || isSgSubtractOp(rhs) ||
        isSgMultiplyOp(rhs) || isSgDivideOp(rhs) ||
        isSgModOp(rhs)) {
      auto binOp = isSgBinaryOp(rhs);
      auto lhs = binOp->get_lhs_operand();
      auto rhs = binOp->get_rhs_operand();
      if (isSgValueExp(lhs) && isSgValueExp(rhs)) {
        return 0;
      } else {
        return 1 + visit_rhs(lhs) + visit_rhs(rhs);
      }
    } else if (isSgUnaryOp(rhs) && !isSgAddressOfOp(rhs) && !isSgPointerDerefExp(rhs)) {
      auto operand = isSgUnaryOp(rhs)->get_operand();
      return ((isSgValueExp(operand) || isSgCastExp(rhs)) ? 0 : 1) + visit_rhs(operand);
    } else {
      if (!isSgValueExp(rhs) && !isSgVarRefExp(rhs) && !isSgPntrArrRefExp(rhs) &&
          !isSgAddressOfOp(rhs) && !isSgPointerDerefExp(rhs)) {
        SageInterface::dumpInfo(rhs);
      }
      return 0;
    }
  }
};

using handle_t   = AbstractHandle::abstract_handle;
using location_t = std::pair<char*, int>;

SgFile* get_file_by_name(SgProject* project, const char* name) {
  auto slash = strrchr(name, '/');
  if (slash) {
    name = slash + 1;
  }
  for (auto file : project->get_files()) {
    auto path = file->getFileName();
    auto pos  = path.length() - strlen(name);
    if (!strcmp(name, path.c_str() + pos)) {
      return file;
    }
  }
  return nullptr;
}

int main (int argc, char* argv[]) {
  std::vector<location_t> locations;
  for (auto i = 0; i < argc; i++) {
    auto colon = strrchr(argv[i], ':');
    if (colon) {
      *colon = '\0';
      locations.push_back(std::make_pair(argv[i], atoi(colon + 1)));
    }
  }

  SgProject* project = frontend( argc, argv );
  ROSE_ASSERT( project );

  SageInterface::changeAllBodiesToBlocks( project );
  RoseVisitor visitor;

  if (!locations.size()) {
    traverseMemoryPoolVisitorPattern( visitor );
  }

  for (auto location : locations) {
    auto file = get_file_by_name( project, location.first );
    ROSE_ASSERT( file );
    auto fNode   = AbstractHandle::buildroseNode(isSgSourceFile(file));
    auto fHandle = new handle_t(fNode);
    handle_t* sHandle = new handle_t(fHandle, "SgNode<position," + std::to_string(location.second) + ">");
    ROSE_ASSERT( sHandle && sHandle->getNode() && "unable to locate statement" );
    auto sNode = (SgNode *)(sHandle->getNode()->getNode());
    SgScopeStatement *encl, *next;
    if (isSgForStatement(sNode)) {
      next = (SgScopeStatement *)sNode;
    } else {
      next = SageInterface::findEnclosingLoop((SgStatement *)sNode);
    }
    do {
      encl = next;
      next = SageInterface::findEnclosingLoop((SgStatement *)encl->get_parent());
    } while (encl != next && next);
    ROSE_ASSERT( encl && "unable to identify enclosing loop" );
    visitor.visit((SgForStatement*)encl);
  }

  return 0;
}

#include <rose.h>
#include <iostream>
#include <fstream>
#include <ctype.h>

using handle_t   = AbstractHandle::abstract_handle;
using location_t = std::tuple<char*, int>;

class LoopCtx
{
  LoopCtx *mParent;
  SgForStatement *mStmt;
  SgFortranDo *mFortranDo;
  SgInitializedName *mIvar;
  SgVariableSymbol *mIterator;
  SgExpression *mLowerBound;
  SgExpression *mUpperBound;
  SgExpression *mStride;
  SgStatement  *mBody;
  int mUid, mDepth;
  std::vector<LoopCtx*> mChildren;
public:
  LoopCtx( SgForStatement* stmt, LoopCtx* parent = nullptr )
  : mStmt(stmt), mParent(parent), mFortranDo(nullptr) {
    SageInterface::forLoopNormalization(mStmt);
    ROSE_ASSERT(SageInterface::isCanonicalForLoop(mStmt));
    SageInterface::getForLoopInformations(mStmt, mIterator, mLowerBound, mUpperBound, mStride);
    mIvar  = mIterator->get_declaration();
    mUid   = ++kUidCtr;
    mDepth = parent ? (parent->mDepth + 1) : 1;
    mBody  = mStmt->get_loop_body();
    if (parent) {
      parent->mChildren.push_back(this);
    }
  }

  LoopCtx( SgFortranDo* stmt, LoopCtx* parent = nullptr )
  : mFortranDo(stmt), mParent(parent), mStmt(nullptr) {
    SageInterface::doLoopNormalization(mFortranDo);
    bool hasIncrementalIterationSpace, isInclusiveUpperBound;
    ROSE_ASSERT(SageInterface::isCanonicalDoLoop(
      mFortranDo, &mIvar,
      &mLowerBound, &mUpperBound, &mStride,
      &mBody, &hasIncrementalIterationSpace, &isInclusiveUpperBound
    ));
    mUid   = ++kUidCtr;
    mDepth = parent ? (parent->mDepth + 1) : 1;
    if (parent) {
      parent->mChildren.push_back(this);
    }
  }

  SgBasicBlock* get_loop_body() const {
    return isSgBasicBlock(mBody);
  }

  LoopCtx* get_child() const {
    if (mChildren.size() == 1) {
      return mChildren[0];
    } else {
      return nullptr;
    }
  }

  std::string find_permutations() const {
    auto curr = this;
    std::stringstream ss;
    ss << "(";
    while (curr->get_child()) {
      ss << curr->mUid << ",";
      curr = curr->get_child();
    }
    ss << curr->mUid << "),";
    for (auto child : curr->mChildren) {
      ss << child->find_permutations();
    }
    return ss.str();
  }

  int get_depth() const {
    return mDepth;
  }

  int get_id() const {
    return mUid;
  }

  std::string to_string() const {
    std::stringstream ss;
    ss << "LoopCtx(" ;
    ss << mIvar->get_name().getString() << ",";
    if (mParent) {
      ss << mParent->to_string();
    } else {
      ss << "NULL";
    }
    ss << ")#" << mUid;
    return ss.str();
  }

  SgNode* get_node() const {
    return mStmt ? (SgNode*)mStmt : (SgNode*)mFortranDo;
  }

  std::string get_location() const {
    auto fNode = AbstractHandle::buildroseNode(get_node());
    auto fName = fNode->getFileName();
    auto idx   = fName.find_last_of("/");
    return fName.substr(idx + 1, std::string::npos) + ":" + std::to_string(fNode->getStartPos().line);
  }

  static int kUidCtr;
};

int LoopCtx::kUidCtr = 0;

std::ostream& operator<<(std::ostream &strm, const LoopCtx &ctx) {
  return strm << ctx.to_string();
}

class RoseVisitor : public ROSE_VisitorPattern
{
public:
  RoseVisitor( ) { }

  void visit( SgFunctionDefinition *fnDef ) {
    // Grab the function's declaration
    SgFunctionDeclaration *fnDecl = fnDef->get_declaration();
    // Find all of the for loops in the function's body
    auto loops = NodeQuery::querySubTree(fnDef->get_body(), V_SgForStatement);
    // For each loop found:
    for (auto node : loops) {
      // Search for an enclosing loop
      auto encl = SageInterface::findEnclosingLoop((SgStatement*)node->get_parent());
      // If one is not found
      if (!encl) {
        // Assume it is a top-level loop and visit the loop nest
        auto ctx = this->visit((SgForStatement*)node, nullptr);
        // Print the permutations
        std::cout << "PERMUTABLE: " << ctx->find_permutations() << std::endl;
      }
    }
  }

  void visit( SgForStatement *stmt, bool precludeOverride = false ) {
    auto curr = stmt;
    SgForStatement* encl = nullptr;
    do {
      encl = (SgForStatement*)SageInterface::findEnclosingLoop((SgStatement*)curr->get_parent());
      if (encl) {
        curr = encl;
      }
    } while (encl);
    auto ctx = visit( curr, nullptr );
    std::cout << "PERMUTABLE: " << ctx->find_permutations() << std::endl;
  }

  void visit( SgFortranDo *stmt, bool precludeOverride = false ) {
    auto curr = stmt;
    SgFortranDo* encl = nullptr;
    do {
      encl = (SgFortranDo*)SageInterface::findEnclosingLoop((SgStatement*)curr->get_parent());
      if (encl) {
        curr = encl;
      }
    } while (encl);
    auto ctx = visit( curr, nullptr );
    std::cout << "PERMUTABLE: " << ctx->find_permutations() << std::endl;
  }

  LoopCtx* visit( SgFortranDo *stmt, LoopCtx *parent ) {
    auto ctx = new LoopCtx(stmt, parent);
    auto body = ctx->get_loop_body()->get_statements();
    std::cout << "LOOP " << ctx->get_id() << ": depth=" << ctx->get_depth() << ",location=" << ctx->get_location() << std::endl;
    for (SgStatement *stmt : body) {
      if (isSgFortranDo(stmt)) {
        this->visit((SgFortranDo*)stmt, ctx);
      }
    }
    return ctx;
  }

  LoopCtx* visit( SgForStatement *stmt, LoopCtx *parent ) {
    auto ctx = new LoopCtx(stmt, parent);
    auto body = ctx->get_loop_body()->get_statements();
    std::cout << "LOOP " << ctx->get_id() << ": depth=" << ctx->get_depth() << ",location=" << ctx->get_location() << std::endl;
    for (SgStatement *stmt : body) {
      if (isSgForStatement(stmt)) {
        this->visit((SgForStatement*)stmt, ctx);
      }
    }
    return ctx;
  }
};

SgFile* get_file_by_name(SgProject* project, const char* name) {
  auto slash = strrchr(name, '/');
  if (slash) {
    name = slash + 1;
  }
  for (auto file : project->get_files()) {
    auto path = file->getFileName();
    auto pos  = path.length() - strlen(name);
    if (!strcmp(name, path.c_str() + pos)) {
      return file;
    }
  }
  return nullptr;
}

int main (int argc, char* argv[]) {
  std::vector<location_t> locations;
  for (auto i = 0; i < argc; i++) {
    auto colon = strrchr(argv[i], ':');
    if (colon) {
      *colon = '\0';
      auto *it = colon + 1;
      while (*it && isdigit(*it)) it++;
      *it = '\0';
      locations.push_back(std::make_tuple(argv[i], atoi(colon + 1)));
    }
  }

  SgProject* project = frontend( argc, argv );
  ROSE_ASSERT( project );

  SageInterface::changeAllBodiesToBlocks( project );
  RoseVisitor visitor;

  if (!locations.size()) {
    traverseMemoryPoolVisitorPattern( visitor );
  }

  for (auto location : locations) {
    auto name = std::get<0>(location);
    auto file = get_file_by_name( project, name );
    bool fortran = name[strlen(name) - 1] == 'f';
    ROSE_ASSERT( file );
    auto fNode   = AbstractHandle::buildroseNode(isSgSourceFile(file));
    auto fHandle = new handle_t(fNode);
    handle_t* sHandle = new handle_t(fHandle, "SgNode<position," + std::to_string(std::get<1>(location)) + ">");
    ROSE_ASSERT( sHandle && sHandle->getNode() && "unable to locate statement" );
    auto sNode = (SgNode *)(sHandle->getNode()->getNode());
    SgScopeStatement *encl, *next;
    if (isSgForStatement(sNode) || isSgFortranDo(sNode)) {
      next = (SgScopeStatement *)sNode;
    } else {
      next = SageInterface::findEnclosingLoop((SgStatement *)sNode);
    }
    do {
      encl = next;
      next = SageInterface::findEnclosingLoop((SgStatement *)encl->get_parent());
    } while (encl != next && next);
    ROSE_ASSERT( encl && "unable to identify enclosing loop" );
    if (fortran) {
      auto doStmt = isSgFortranDo(encl);
      ROSE_ASSERT( doStmt );
      visitor.visit(doStmt);
    } else {
      auto forStmt = isSgForStatement(encl);
      ROSE_ASSERT( forStmt );
      visitor.visit(forStmt);
    }
  }

  return 0;
}

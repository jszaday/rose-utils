#include <rose.h>
#include <iostream>

Sg_File_Info *kFileInfo;
SgNode       *kRootNode;

class RoseVisitor : public ROSE_VisitTraversal
{
public:
  int numAssigns;   // ft21
  int numIntBinOps; // ft22
  int numFltBinOps; // ft23
  int numPtrFunCalls, numIntFunCalls, numFltFunCalls; // ft44, ft45
  int numBinOpsWithIntVals; // ft41
  int numIntVals; // ft47 + ft49
  int numZeroes;  // ft46
  int numOnes;    // ft48

  RoseVisitor()
  : numAssigns(0), numIntBinOps(0), numFltBinOps(0), numPtrFunCalls(0),
    numIntFunCalls(0), numFltFunCalls(0), numBinOpsWithIntVals(0),
    numZeroes(0), numOnes(0), numIntVals(0) {

  }

  void visit( SgNode* node ) {
    Sg_File_Info* nodeInfo = node->get_file_info();

    if ( !(nodeInfo && kFileInfo->isSameFile(nodeInfo)) ) {
      return;
    } else if ( isSgBinaryOp(node) ) {
      SgBinaryOp   *binOp = (SgBinaryOp*) node;
      SgExpression *lhs   = binOp->get_lhs_operand();
      SgExpression *rhs   = binOp->get_rhs_operand();

      if (binOp->get_type()->isIntegerType()) {
        numIntBinOps++;
      } else if (binOp->get_type()->isFloatType()) {
        numFltBinOps++;
      }

      if ( isSgCompoundAssignOp(binOp) || isSgAssignOp(binOp) ) {
        numAssigns++;
      }

      if ( isSgIntVal(lhs) || isSgIntVal(rhs) ) {
        numBinOpsWithIntVals++;
      }

      std::cout << "Encountered a bin op of type " << binOp->class_name() << " on line " << nodeInfo->get_line() << std::endl;
    } else if ( isSgFunctionCallExp(node) ) {
      SgType *type = ((SgFunctionCallExp*) node)->get_type();

      if (isSgPointerType(type)) {
        numPtrFunCalls++;
      } else if (type->isIntegerType()) {
        numIntFunCalls++;
      } else if (type->isFloatType()) {
        numFltFunCalls++;
      }
    } else if ( isSgIntVal(node) ) {
      const int val = ((SgIntVal*) node)->get_value();

      numIntVals++;

      if (val == 0) {
        numZeroes++;
      } else if (val == 1) {
        numOnes++;
      }
    }
  }
};

int main (int argc, char* argv[]) {
  SgProject* project = frontend(argc, argv);
  ROSE_ASSERT( project != NULL );

  kFileInfo = project->get_file(0).get_file_info();
  kRootNode = project;

  SageInterface::changeAllBodiesToBlocks( project );

  RoseVisitor visitor;
  visitor.traverseMemoryPool();

  std::cout << "Counted " << visitor.numAssigns   << " Assignment Statements (outside of declarations)." << std::endl;
  std::cout << "Counted " << visitor.numIntBinOps << " Integer Bin Ops." << std::endl;
  std::cout << "Counted " << visitor.numFltBinOps << " Floating Point Bin Ops." << std::endl;
  std::cout << "Counted " << visitor.numIntVals << " int values where " << visitor.numZeroes << " were zero and " << visitor.numOnes << " were one." << std::endl;
  std::cout << visitor.numBinOpsWithIntVals << " bin ops operated on these constants." << std::endl;

  return backend( project );
}
